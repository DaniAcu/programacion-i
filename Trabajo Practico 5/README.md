
# Trabajo Practico 5
*Programación I*

## Tarea 1:
Leer una matriz entera de NxM, la mayoría de sus elementos son cero, se pide:
 - Leer la matriz y almacenar los elementos significativos (distintos de 0) y su posición (fila/columna) en tres vectores respectivamente.
 - Mostrar por pantalla el número de fila con mayor cantidad de elementos significativos.

## Tarea 2:
Desarrollar un programa que lea una matriz de NxM, un vector de N elementos, y que determine si existen filas cuyo promedio coincide con los elementos almacenados en el vector (promedio fila K = vector K). En el caso de que exista, escribir el o los número de fila).

## ¿Como probar?
Usando el script *run* puede ejecutar todos los programas.

`./run 'Tarea 1'`

Si desea volver a compilar puede usar.

`./compile 'Tarea 2'`

Si estas en **Windows** vas a tener que usar el comando bash para correr estos.
Usando el *cmd* podes correr los comandos asi:

`bash ./run 'Tarea 1'`
-----
`bash ./compile 'Tarea 2'`


