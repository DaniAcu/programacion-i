
  #include <stdio.h>
  #include <stdlib.h>

  int main() {
    int rows, columns, i, j;

    printf("Ingrese N, siendo N la cantidad de filas de la matriz\n");
    scanf("%d", &rows);
    printf("Ingrese M, siendo M la cantidad de columnas de la matriz\n");
    scanf("%d", &columns);

    printf("=========================\n");
    printf("Lectura de Matriz\n");
    printf("=========================\n");

    int matrix[rows][columns], amountValidItem = 0;

    for (i = 0; i < rows; i++) {
      for (j = 0; j < columns; j++){
        int aux = 0;
        printf("Ingresar valor ha almacenar en la posicion %dx%d\n", i, j);
        scanf("%d", &aux);
        if(aux > 0) amountValidItem++;
        matrix[i][j] = aux;
        fflush(stdin);
      }
    }


    int itemList[amountValidItem], itemNRefs[amountValidItem], itemMRefs[amountValidItem], index = 0;

    for (i = 0; i < rows; i++) {
      for (j = 0; j < columns; j++){       
        if(matrix[i][j] > 0) {
          itemList[index] = matrix[i][j];
          itemNRefs[index] = i;
          itemMRefs[index] = j;
          index++;
        }
      }
    }

    int rowMostUsed = 0, maxUsesOfRow = 0;
    

    for (int idxItem = 0; idxItem < amountValidItem; idxItem++) {
      int count = 1;
      
      for (int subIdxItem = 1; subIdxItem < amountValidItem; subIdxItem++) {
        if(itemNRefs[idxItem] == itemNRefs[subIdxItem]) count++;
      }

      if(maxUsesOfRow < count) {
        rowMostUsed = itemNRefs[idxItem];
        maxUsesOfRow = count;
      }
    }


    printf("\nLa fila con mayor cantidad de elementos significativos es la %d\n", rowMostUsed);   


    return 0;
  }

