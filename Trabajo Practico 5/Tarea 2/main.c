
  #include <stdio.h>
  #include <stdlib.h>

  int main() {
    int rows, columns, i, j;

    printf("Ingrese N, siendo N la cantidad de filas de la matriz\n");
    scanf("%d", &rows);
    printf("Ingrese M, siendo M la cantidad de columnas de la matriz\n");
    scanf("%d", &columns);

    printf("=========================\n");
    printf("Lectura de Matriz\n");
    printf("=========================\n");

    int matrix[rows][columns];
    float averageArray[rows];

    for (i = 0; i < rows; i++) averageArray[i] = 0;

    for (i = 0; i < rows; i++){
      for (j = 0; j < columns; j++){
        int aux = 0;
        printf("Ingresar valor ha almacenar en la posicion %dx%d\n", i, j);
        scanf("%d", &aux);
        matrix[i][j] = aux;

        averageArray[i] += matrix[i][j];
        
        fflush(stdin);
      }
      averageArray[i] = averageArray[i] / rows;
    }

    printf("=========================\n");
    printf("Lectura de Vector\n");
    printf("=========================\n");

    int array[rows] ;

    for (i = 0; i < rows; i++) {
        printf("Ingresar valor ha almacenar en la posicion %d\n", i);
        scanf("%d", &array[i]);
        fflush(stdin);
    }

    for (i = 0; i < rows; i++) {
        if(averageArray[i] == array[i]) {
            printf("El promedio coincide con el valor en el vector en la fila %d\n", i);
        }
    }



    return 0;
  }

