
  #include <stdio.h>
  #include <stdlib.h>

  #define MONTH_LENGTH 12

  int main() {
    const char months[MONTH_LENGTH][11] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
    float harvestAmount[MONTH_LENGTH], totalHarvestAmount = 0, averageHarvest, bestHarvest = 0;
    int betterThanAverage = 0, totalBestHarvest = 0;

    printf("Ingrese las toneladas cosechadas por cada mes.\n");

    for (int monthId = 0; monthId < MONTH_LENGTH; monthId++) {
      float monthAmount = 0;
      do {
        if (monthAmount < 0) printf("Valor invalido, por favor ingrese nuevamente.\n");
        printf("%s : ", months[monthId]);
        scanf("%f", &monthAmount);
      } while(monthAmount < 0);

      harvestAmount[monthId] = monthAmount;
      totalHarvestAmount += monthAmount;
      if(bestHarvest < monthAmount) bestHarvest = monthAmount;
    }

    printf("\n");

    printf("=================================================\n\n");

    averageHarvest = totalHarvestAmount / MONTH_LENGTH;

    printf("A) Promedio anual de cosecha: %.2f\n", averageHarvest);

    printf("B) Mejor(es) mes(es) de %.2f toneladas: ", bestHarvest);

    for (int monthId = 0; monthId < MONTH_LENGTH; monthId++) {
      if(harvestAmount[monthId] > averageHarvest) betterThanAverage++;
      if(harvestAmount[monthId] == bestHarvest) {
        printf("%s ", months[monthId]);
        totalBestHarvest++;
      }
    }
    printf("\n Total de mejores meses: %d\n", totalBestHarvest);

    printf("C) Cantidad de meses mayores al promedio: %d", betterThanAverage);   
    
    return 0;
  }

