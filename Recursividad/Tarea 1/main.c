#include <stdio.h>
#include <stdlib.h>

void showSumOperation(int num, int i);

int main() {
  int num;

  do {
    printf("Ingrese un numero:");
    scanf("%d", &num);
    printf("\n");

    if (num <= 0) printf("Valor ingresado invalido. Ingrese valores naturales\n");
    else showSumOperation(num, 0);

  } while (num <= 0);

  

  return 0;
}

void showSumOperation(int num, int i) {
  int a = num - 1;
  int b = i + 1;
  
  printf(" %d + %d |", a, b);

  if((a - 1) > b) showSumOperation(a, b);
}
