#include <stdio.h>
#include <stdlib.h>
#define LENGTH 4

void showPairs (char letter, char *list, int i, int lenght);
void generatePair(char *str, int i, int lenght);

int main() {
  char list[LENGTH] = { 'M', 'P', 'R', 'T' };

  generatePair(list, 0, LENGTH);

  return 0;
}

void showPairs (char letter, char *list, int i, int lenght) {
  if(i < lenght - 1) {
    printf("[%c,%c]", letter, list[i+1]);
    showPairs(letter, list, i + 1, lenght);
  } else {
    printf("\n");
  }
}

void generatePair(char *list, int i, int lenght) {
  if(i < lenght) {
    showPairs(list[i], list, i, lenght);
    generatePair(list, i + 1, lenght);
  }  
}
