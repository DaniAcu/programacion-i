
  #include <stdio.h>
  #include <stdlib.h>

  int main() {
    int category;
    float payment, packageWeight, categoryModifier, overloadModifier;

    printf("Ingresar peso de su paquete (kg): ");
    scanf("%f", &packageWeight);

    if(packageWeight <= 0) {
      printf("El valor ingresado no es valido!\n");
      exit(1);
    }

    printf("Categorias:");
    printf("\n 1. comun\n 2. especial\n 3. aereo\n");
    printf("Seleccione la categoria de su paquete usando su identificador numerico: ");
    scanf("%d", &category);

    switch (category) {
      case 1:
        categoryModifier = 2;
      break;

      case 2:
        categoryModifier = 2.5;
      break;

      case 3:
        categoryModifier = 3;
      break;

      default:
        printf("La opcion seleccionada no es valida!\n");
        exit(1);
      break;
    }

    if(packageWeight > 5 && packageWeight <= 10) {
      overloadModifier = (packageWeight / 100) * 10;
    } else if(packageWeight > 10 && packageWeight <= 15) {
      overloadModifier = (packageWeight / 100) * 20;
    } else {
      overloadModifier = (packageWeight / 100) * 30;
    }

    payment = categoryModifier * packageWeight + overloadModifier;

    printf("Su importe a pagar es de $%.2f", payment);
  
    return 0;
  }

