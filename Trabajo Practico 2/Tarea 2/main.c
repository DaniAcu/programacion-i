
  #include <stdio.h>
  #include <stdlib.h>

  int main() {
    float imc, weight, height;

    printf("Ingrese peso (kg): ");
    scanf("%f", &weight);
    printf("Ingrese altura (m): ");
    scanf("%f", &height);

    if(weight < 0 || height < 0) {
      printf("Los valores ingresados no son validos!\n");
      exit(1);
    }

    imc = weight / (height * height); // Also we could include math.h and use pow. e.g. pow(height, 2)

    printf("Resultado de indice de masa corporal: %.2f\n", imc);

    if(imc <= 17) {
      printf("\nDelgadez extrema\n");
    } else if (imc > 17 && imc <= 20) {
      printf("\nPeso bajo\n");
    } else if (imc > 20 && imc <= 24) {
      printf("\nSaludable\n");
    } else if (imc > 24 && imc <= 30) {
      printf("\nSobrepeso\n");
    } else {
      printf("\nObesidad\n");
    }

    return 0;
  }

