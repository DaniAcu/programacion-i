
  #include <stdio.h>
  #include <stdlib.h>

  int main() {
    int type, pulses;
    float payment = 0, pulsesValue;

    printf("Tipo de abonados\n");
    printf("1. Particular\n2. Profesional\n3. Comercial\n\n");
    printf("Seleccione el tipo de abonado ingresando el identificador numerico: ");
    scanf("%d", &type);

    switch (type) {
      case 1:
        payment += 30;
        break;
      case 2:
        payment += 50;
        break;
      case 3:
        payment += 70;
        break;
      
      default:
        printf("Opcion no valida, vuelva más tarde\n");
        exit(1);
        break;
    }

    printf("Ingrese cantidad de pulsos: ");
    scanf("%d", &pulses);

    if(pulses < 0){
      printf("Opcion no valida, los pulsos deben ser mayores a 0. Vuelva más tarde.\n");
      exit(1);
    }

    switch (type) {
      case 1:
        if(pulses >= 0 && pulses <= 200){
          pulsesValue = 0.05;
        } else if(pulses >= 201 && pulses <= 400){
          pulsesValue = 0.07;
        } else if(pulses >= 401 && pulses <= 1000){
          pulsesValue = 0.1;
        } else {
          pulsesValue = 0.12;
        }
      break;
      case 2:
        if(pulses >= 0 && pulses <= 250){
          pulsesValue = 0.07;
        } else if(pulses >= 251 && pulses <= 500){
          pulsesValue = 0.11;
        } else if(pulses >= 501 && pulses <= 1000){
          pulsesValue = 0.13;
        } else {
          pulsesValue = 0.15;
        }
      break;
      case 3:
        if(pulses >= 0 && pulses <= 300){
          pulsesValue = 0.9;
        } else if(pulses >= 301 && pulses <= 600){
          pulsesValue = 0.12;
        } else if(pulses >= 601 && pulses <= 1000){
          pulsesValue = 0.15;
        } else {
          pulsesValue = 0.17;
        }
      break;
    }

    payment += pulses * pulsesValue;
    
    printf("\n===========================\n Importe a pagar: $%.2f\n===========================\n", payment);

    return 0;
  }

