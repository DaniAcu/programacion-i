
# Trabajo Practico 2
*Programación I*

## Tarea 1:
Una empresa telefónica factura de la siguiente manera para cada uno de los tipos de abonados que
se detallan a continuación: 1-Particular 2- Profesional 3- Comercial Tienen abonos de 30, 50 y 70
pesos respectivamente Además el valor del pulso para cada categoría es: 

| | | | | |
|-|-|-|-|-|
|  particular | 0-200 | 201-400 | 401-1000 | más de 1000 |
|             |  0.05 |   0.07  |    0.1   |     0.12    |
| Profesional | 0-250 | 251-500 | 501-1000 | más de 1000 |
|             |  0.07 |   0.11  |   0.13   |     0.15    |
|  Comercial  | 0-300 | 301-600 | 601-1000 | más de 1000 |
|             |  0.09 |   0.12  |   0.15   |     0.17    |

Se pide: Ingresar categoría y cantidad de pulsos para un abonado y determinar el importe a pagar
por él.

## Tarea 2:
Realizar un programa en C que permita calcular índice de masa corporal actual:
Peso/Altura² e indicar:
• inferior o igual a 17: “Delgadez extrema”
• mayor a 17 a 20 inclusive: “Peso bajo”
• mayor a 20 a 25 inclusive “Saludable”
• mayor a 25 a 29 inclusive “Sobrepeso”
• mayor a 30 “Obesidad”

## Tarea 3:
Una empresa de transporte realiza varios envíos de cargas, se desea calcular el importe a pagar.
- Los datos que se ingresan son PESO de la carga, es un número real y CATEGORÍA es un dato codificado: 1 = común, 2 = especial, 3 = aéreo.
- El precio se calcula de
  - $2 por kilo de peso para categoría común,
  - $2.5 por kilo para categoría y
  - $3 por kilo para categoría aérea.
- Se cobra recargo por sobrepeso:
  - 30% si sobrepasa los 15 kilos,
  - 20% si pesa mas e 10 kilos y hasta 15 kilos inclusive,
  - 10% si pesa mas de 5 kilos y hasta 10 kilos inclusive.


## ¿Como probar?
Usando el script *run* puede ejecutar todos los programas.

`./run 'Tarea 1'`

Si desea volver a compilar puede usar.

`./compile 'Tarea 2'`

Si estas en **Windows** vas a tener que usar el comando bash para correr estos.
Usando el *cmd* podes correr los comandos asi:

`bash ./run 'Tarea 1'`
-----
`bash ./compile 'Tarea 2'`


