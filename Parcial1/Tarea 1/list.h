#include "client.h"

typedef struct node {
  Client value;
  struct node *next;
  struct node *prev;
} Node;

typedef struct list {
  Node *first;
  Node *last;
} List;

typedef struct clientSaleNode {
  int id;
  struct clientSaleNode *next;
  struct saleNode *sales;
} ClientSaleNode;

typedef struct saleNode {
  int date;
  float price;
  struct saleNode *next;
} SaleNode;

ClientSaleNode *createClientSaleNode(int id, SaleNode *sales);
void addClientNode(ClientSaleNode **head, int id, SaleNode *sales);
void showSubList(ClientSaleNode *head);

Node *createNode (Client value);
List *createList();
void addNode(List *list, Client value);
void showList(List list);
void fillList (List *list);
void buildSubList(List list, ClientSaleNode **head);
