#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "file.h"

#define FILENAME_CSV "client.csv"

ClientSaleNode * createClientSaleNode(int value, SaleNode *sales) {
    ClientSaleNode *node = malloc(sizeof(ClientSaleNode));
    node->id = value;
    node->next = NULL;
    node->sales = sales;

    return node;
}

SaleNode * createSaleNode(int date, float price) {
    SaleNode *node = malloc(sizeof(SaleNode));
    node->date = date;
    node->price = price;
    node->next = NULL;

    return node;
}

void addClientNode(ClientSaleNode **head, int id, SaleNode *sales) {
    ClientSaleNode *newNode = createClientSaleNode(id, sales);
    newNode->next = *head;
    *head = newNode;
}

void addSaleNode(SaleNode **head, int date, float price) {
    SaleNode *newNode = createSaleNode(date, price);
    newNode->next = *head;
    *head = newNode;
}

Node *createNode (Client value) {
    Node *pNode = malloc(sizeof(Node));
    pNode->value = value;
    pNode->next = NULL;
    pNode->prev = NULL;

    return pNode;
}

List *createList () {
    List *list = malloc(sizeof(List));
    list->first = NULL;
    list->last = NULL;

    return list;
}

// add delete all;

void addNode(List *list, Client value) {
    Node *newNode = createNode(value);
    
    if(list->first == NULL && list->last == NULL) {
        list->first = newNode;
        list->last = newNode;
    } else {
        newNode->next = list->first;
        list->first->prev = newNode;
        list->first = newNode;
    }
}

void showList(List list) {
    Node *node = list.last;
    while (node != NULL){
        printf("%d - %s - %d - %.2f", node->value.id, node->value.name, node->value.date, node->value.price);
        printf("\n");
        node = node->prev;
    }
}

void fillList (List *list) {
  FILE *clientsFileCSV = openFile(FILENAME_CSV, "r");
    
  if(clientsFileCSV != NULL){
    Client client;
    int day, month, year, id;
    char name[20];
    float price;

    
    // 1001,Daniel Acuña,31/08/1998,48.5

    while(feof(clientsFileCSV) == 0){
        fscanf(clientsFileCSV, "%d,%[^,],%d/%d/%d,%f\n", &id, name, &day, &month, &year, &price);
        strcpy(client.name, name);
        client.id = id;
        client.date = (year * 10000) + (month * 100) + day;
        client.price = price;
        addNode(list, client);
    }
    fclose(clientsFileCSV);
  }
};


void buildSubList(List list, ClientSaleNode **head) {
    Node *node = list.first;
    while (node != NULL){
        int isAlreadySaved = 0;
        ClientSaleNode *currentSaleNode = *head;
        while (currentSaleNode != NULL){
            if(currentSaleNode->id == node->value.id) {
                isAlreadySaved = 1;
            }
            currentSaleNode = currentSaleNode->next;
        }

        if(isAlreadySaved == 0) {
            SaleNode *sales = NULL;
            Node *currentNode = list.first;
            while (currentNode != NULL){
                if(currentNode->value.id == node->value.id){
                    addSaleNode(&sales, currentNode->value.date, currentNode->value.price);
                }
                currentNode = currentNode->next;
            }
            
            addClientNode(head, node->value.id, sales);
        } 

        node = node->next;
    }
}

void showSubList(ClientSaleNode *head) {
    printf("\n");
    printf("\n");
    ClientSaleNode *node = head;
    while (node != NULL){
        printf("Cliente %d:\n", node->id);
        printf("Ventas:\n");
        SaleNode *currentSaleNode = node->sales;
        while (currentSaleNode != NULL){
            int year = currentSaleNode->date / 10000,
                month = (currentSaleNode->date % 10000) / 100,
                day = (currentSaleNode->date % 10000) % 100;
            printf("Fecha: %d/%d/%d \n", day, month, year);
            printf("Precio: $%.2f \n", currentSaleNode->price);

            printf("\n");

            currentSaleNode = currentSaleNode->next;
        }
        
        node = node->next;
    }
    printf("\n");
    printf("\n");
}
