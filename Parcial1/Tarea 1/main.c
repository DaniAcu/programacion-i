
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int main() {
  List *list = createList();
  ClientSaleNode *clientSalesList = NULL;
  fillList(list);

  showList(*list);

  buildSubList(*list, &clientSalesList);

  showSubList(clientSalesList);

  return 0;
}

