
  #include <stdio.h>
  #include <stdlib.h>

  int main() {
    const int MINIMUM_BALANCE = -50000;
    printf("===================");
    printf("\n  Cuenta Bancaria\t\n");
    printf("===================\n");

    float balance, amount = 1;
    int movementType, extractions = 0, deposits = 0;

    printf("Por favor ingresar el SALDO INICIAL: ");
    scanf("%f", &balance);
    printf("\n");

    do {
      printf("Tipo de Movimiento (1) Extraccion (2) Deposito: ");
      scanf("%d", &movementType);
      printf("\n");
      
      if(movementType != 1 && movementType != 2) {
        printf("OPTION INVALIDA. Por valor ingrese 1 para extraccion o 2 para deposito.\n");
      } else {
        printf("Ingresar importe (0 para salir): ");
        scanf("%f", &amount);
        printf("\n");

        if(amount != 0){

          switch (movementType) {
            case 1:
              if((balance - amount) >= MINIMUM_BALANCE) {
                balance -= amount;
                extractions++;
              } else {
                printf("No puede realizar la extraccion. Solamente puede extraer $%.2f\n", (-1 * MINIMUM_BALANCE) - balance);
              }
            break;
            case 2:
              balance += amount;
              deposits++;
            break;
          }

        }
      }
    } while(amount != 0);

    printf("Saldo total: %.2f\n\n", balance);
    printf("Cantidad de extracciones: %d\n", extractions);
    printf("Cantidad de depositos: %d\n", deposits);

    return 0;
  }

