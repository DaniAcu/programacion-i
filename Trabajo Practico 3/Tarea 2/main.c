
  #include <stdio.h>
  #include <stdlib.h>

  int main() {
    int user, password, amountOfFails = 0, students, approvedStudents = 0;
    printf("Sistema de Procesamiento de Notas\n");
    printf("================\n");

    do {
      printf("Usuario: ");
      scanf("%d", &user);
      printf("Contraseña: ");
      scanf("%d", &password);
      amountOfFails++;
    } while((user != 2020 || password != 123) && amountOfFails < 3);

    printf("================\n\n");

    if(user == 2020 && password == 123){
      printf("Bienvenido %d!\n\n", user);
      do {
        printf("Ingrese la cantidad de alumnos: ");
        scanf("%d", &students);
        if(!(students >= 1 && students <= 50)) {
          printf("\nEl valor ingresado NO es valido. Solo se permiten ingresar hasta 50 alumnos.\n");
        }
      } while(!(students >= 1 && students <= 50));
      
      for (int student = 1; student <= students; student++){
        int amountOfGradePoints = 0;
        float gradePoint = 0, totalGradePoints = 0;
        printf("\n============================================================================================\n");
        printf("\tESTUDIANTE N°%d\n\n", student);

        printf("\tIngrese nota de estudiante (entre 1 y 10, -1 finaliza): ");
        scanf("%f", &gradePoint);

        /* 
          Nota para el profesor: 
            Uso de la estructura 'while', para poner en practica los temas aprendidos.
            Podria usarse do while y evitar repetir el scanf, ambas soluciones funcionarian.
        */

        while(gradePoint != -1) {
          if(gradePoint >= 1 && gradePoint <= 10) {
            totalGradePoints += gradePoint;
            amountOfGradePoints++;
          } else {
            printf("\tValor invalido! Ingrese un valor ente 1 y 10. O -1 para finalizar.\n");
          }

          printf("\tIngrese nota de estudiante (entre 1 y 10, -1 finaliza): ");
          scanf("%f", &gradePoint);
        };

        float studentAverage = totalGradePoints / amountOfGradePoints;

        if(studentAverage >= 7) {
          approvedStudents++;
        }

        printf("\n\tPromedio del estudiante: %.2f\n", studentAverage);

        printf("\n============================================================================================\n");
      }

      printf("\nCantidad de alumnos aprovados: %d.\n", approvedStudents);

    } else {
      printf("Usted ha superado la cantidad de intentos para ingresar al sistema. Vuelva más tarde.");
    }

    return 0;
  }

