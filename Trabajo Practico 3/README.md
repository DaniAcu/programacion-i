
# Trabajo Practico 3
*Programación I*

## Tarea 1:
Un banco quiere llevar control del estado de cuenta de un cliente.
Para ello el operario ingresa el saldo inicial de la cuenta y a continuación una cantidad no conocida
de antemano de movimientos.
Para cada uno de dichos movimientos se ingresa el tipo (1 = extracción o 2 = depósito) y el importe
(el importe 0 indica fin de ingreso de datos).
La cuenta puede tener saldo negativo de hasta $50.000 para girar en descubierto. En el caso de no
tener saldo suficiente, mostrar un cartel indicatorio por pantalla.
Se pide indicar el estado final de la cuenta del cliente.
**Aclaración: no usar vectores, solo se permite uso de variables enteras y de punto flotante.**
**Se tendrán en cuenta las protecciones según los límites explicitados en el ejercicio**

Diseñe un programa que permita validar los datos de usuario (entero) y clave (entero) para elingreso a un Sistema de Procesamiento de Notas (usuario = 2020; clave = 123).
Si el usuario comete más de 3 errores durante la entrada de sus datos, el sistema envía el mensaje
"supero el número de oportunidades, Vuelva otro día" y luego finaliza.
Si el usuario logra ingresar al sistema, éste le solicita que ingrese el número de alumnos, mínimo 1,
máximo 50.
Luego, por cada alumno debe ingresar las notas finales **(puntaje entre 1 y 10)** de los cursos que
han realizado en el ciclo 2019 (el ingreso de las notas del alumno termina cuando se ingresa el valor
-1).
**El sistema debe calcular y mostrar por cada alumno, la nota promedio que ha obtenido en el**
**ciclo 2019 y finalmente el número de aprobados según dicho promedio. (Se considera**
**aprobado, promedio >= 7).**
**Aclaración: no usar vectores, solo se permite uso de variables enteras y de punto flotante. Se**
**tendrán en cuenta las protecciones según los límites explicitados en el ejercicio**

## ¿Como probar?
Usando el script *run* puede ejecutar todos los programas.

`./run 'Tarea 1'`

Si desea volver a compilar puede usar.

`./compile 'Tarea 2'`

Si estas en **Windows** vas a tener que usar el comando bash para correr estos.
Usando el *cmd* podes correr los comandos asi:

`bash ./run 'Tarea 1'`
-----
`bash ./compile 'Tarea 2'`


