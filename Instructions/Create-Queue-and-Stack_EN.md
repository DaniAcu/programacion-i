# Create Queue and Stack
Follow the next instruction to build a Queue and a Stack

**Before anything, create a Simple Linked List**
> Note: Use primitive values into the node (`char`, `int`, `float` or any other)

Create a `show` method to have the possibility to see the structure.

**IMPORTANT: NOT USE ANY LOOPS (`for`, `while`, `do .. while` etc)**

# Queue
Check the visualization
![Queue GeekForGeek](https://media.geeksforgeeks.org/wp-content/cdn-uploads/gq/2014/02/Queue.png)

**This structure is FIFO: Fist in, first Out**

On your program you should create just the following methods:
- `enqueue()` − add an item to the queue.
- `dequeue()` − remove an item from the queue.
- `peek()` − Gets the element at the front of the queue without removing it.
- `isEmpty()` − Checks if the queue is empty.

# Stack
Check the visualization
![Stack GeekForGeek](https://media.geeksforgeeks.org/wp-content/cdn-uploads/gq/2013/03/stack.png)

**This structure is LIFO: Last in, first out**

On your program you should create just the following methods:
- `push()` − Pushing an element on the stack.
- `pop()` − Removing an element from the stack.
- `peek()` − Get the top data element of the stack, without removing it.
- `isEmpty()` − check if stack is empty.
