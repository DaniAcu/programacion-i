# Crear Cola y Pila
Sigue las siguiente instrucción para construir una cola y una pila

**Antes que nada, cree una Lista Enlazada Simple**
> Nota: use valores primitivos en el nodo (`char`, `int`, `float` o cualquier otro)


Luego de construir las estructuras crea una function `show` (o  modificar la ya creada para la Lista Enlazada Simple).

**IMPORTANTE: NO UTILICE NINGÚN BUCLE (`for`, `while`, `do .. while` etc)**

# Cola
Observe la visualizacion de la estructura
![Queue GeekForGeek](https://media.geeksforgeeks.org/wp-content/cdn-uploads/gq/2014/02/Queue.png)

**Esta estructura es FIFO: Fist in, first out (primero en entrar, primero en salir)**

En su programa, debe crear solo los siguientes métodos o funciones:
- `enqueue ()`- agrega un elemento a la cola.
- `dequeue ()` - elimina un elemento de la cola.
- `peek ()` - Obtiene el elemento al principio de la cola sin eliminarlo.
- `isEmpty ()`- Comprueba si la cola está vacía.

# Pila
Observe la visualizacion de la estructura
![Stack GeekForGeek](https://media.geeksforgeeks.org/wp-content/cdn-uploads/gq/2013/03/stack.png)

**Esta estructura es LIFO: Last in, first out (último en entrar, primero en salir)**

En su programa, debe crear solo los siguientes métodos:
- `push ()` - Empujar un elemento en la pila.
- `pop ()` - Eliminando un elemento de la pila.
- `peek ()` - Obtiene el elemento de datos superior de la pila, sin eliminarlo.
- `isEmpty ()` - comprueba si la pila está vacía.
