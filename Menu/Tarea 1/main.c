#include <ncurses.h>
#define ENTER 10

int getArrayLength (char **arrayStr) {
  int length = -1;
  while (arrayStr[++length] != NULL);
  return length;
}

void renderMenu (int option, char **options) {
  clear();
  start_color();
  init_pair(1, COLOR_GREEN, -1);
  init_pair(2, -1, -1);
  printw("\n");
  attron(COLOR_PAIR(2));

  int totalOptions = getArrayLength(options);

  printw("Select a option using arrow and enter. (if arrows doesn't work click on screen)\n");

  for(int i=0; i<totalOptions; i++){
    if(option == i + 1) attron(COLOR_PAIR(1));
    printw("%s %s\n", option == i + 1 ? ">" : " ", options[i]);
    attron(COLOR_PAIR(2));
  }
}

int getOption (char **options) {
  int key;
  int option = 1;

  const int limitOptions = getArrayLength(options);

  /* Curses Initialisations */
  initscr();
  raw();
  keypad(stdscr, TRUE);
  noecho();
  use_default_colors();

  do
  {
    switch (key)
    {
      case KEY_UP: 
        if(option > 1) option -= 1;
        else option = limitOptions;        
      break;
      case KEY_DOWN:
        if(option < limitOptions) option += 1;
        else option = 1;
      break;
    }
    renderMenu(option, options);
  }while ((key = getch()) != ENTER);
  endwin();

  return option;
}

int main()
{
  char *options[3] = {"Cargar lista", "Leer Lista", "Remover node", NULL};
  int optionSelected = getOption(options);
  printf("\nOption selected: %d\n", optionSelected);

  return 0;
}