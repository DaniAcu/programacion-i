#include <stdio.h>
#include <stdlib.h>

// conio.h just work on Window system
void getch () {
  while(getchar()!='\n');
  getchar();
}
// That small function is supported by all OS (Window, Linux and Mac). 

void Tarea_1();
void Tarea_2();
void Tarea_3();
void Tarea_4();
void Tarea_default();
int getOption();
void pressAnyKey();

int main()
{
  int opcion;
  while (opcion = getOption())
  {
    switch (opcion)
    {
    case 1:
      Tarea_1();
      break;
    case 2:
      Tarea_2();
      break;
    case 3:
      Tarea_3();
      break;
    case 4:
      Tarea_4();
      break;
    case 0:
      printf("El programa ha finalizado.\n");
      break;
    default:
      Tarea_default();
      break;
    }

    pressAnyKey();
  }
  return 0;
}
int getOption()
{
  int opcion;
  do
  {
    //system("@cls||clear");
    printf("\n\n=========== Funciones - GUIA 1 ===========\n \n");
    printf("Elija un tarea:\n");
    printf("\t 1: Mostrar Saludo 1\n");
    printf("\t 2: Mostrar Saludo 2\n");
    printf("\t 3: Cargar nombre\n");
    printf("\t 4: Cargar fecha de nacimiento\n");
    printf("0: SALIR\n");
    scanf("%d", &opcion);
  } while (opcion < 0);
  //system("@cls||clear");
  return opcion;
}

void pressAnyKey () {
  printf("\nPresione una tecla para volver al menu ...");
  getch();
}

void Tarea_1 () {
  printf("Hola...\n");
}

void Tarea_2 () {
  printf("Bienvenido...\n");
}

void Tarea_3 () {
  char name[20];
  printf("Ingrese su nombre: ");
  scanf("%s", &name);
  printf("\n\nSu nombre es %s\n\n", name);
}

void Tarea_4 () {
  int day, month, year;
  printf("Ingrese su fecha de nacimiento con el formato dd/mm/aaaa: ");
  scanf("%2d/%2d/%4d", &day, &month, &year);
  printf("\n\nSu nombre edad es %d años\n\n", 2020 - year);
}

void Tarea_default () {
  printf("Opción incorrecta\n");
}