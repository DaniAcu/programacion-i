#include <stdio.h>
#include <stdlib.h>

void buildMatrix(int matrix[10][10], int *row, int *column);
void showMatrix(int matrix[10][10], int row, int column);
void showMainDiagonal(int matrix[10][10], int row, int column);
void showSecondDiagonal(int matrix[10][10], int row, int column);
void menu(int matrix[10][10], int *row, int *column);
int getOption();

void main(){
  int row = 0, column = 0, matrix[10][10];

  menu(matrix, &row, &column);
}

void menu(int matrix[10][10], int *row, int *column) {
  int opcion;
  while (opcion = getOption()) {
    switch (opcion) {
      case 1:
        buildMatrix(matrix, row, column);
        break;
      case 2:
        showMatrix(matrix, *row, *column);
        break;
      case 3:
        showMainDiagonal(matrix, *row, *column);
        break;
      case 4:
        showSecondDiagonal(matrix, *row, *column);
        break;
      case 0:
        printf("El programa ha finalizado.\n");
      default:
        break;
    }
  }
}

int getOption() {
  int opcion;
  do
  {
    printf("\n\n=========== Funciones ===========\n \n");
    printf("Elija un tarea:\n");
    printf("\t 1: Cargar Matriz\n");
    printf("\t 2: Mostrar Matriz\n");
    printf("\t 3: Mostrar Diagonal Primaria\n");
    printf("\t 4: Mostrar Diagonal Secundaria\n");
    printf("0: SALIR\n");
    scanf("%d", &opcion);
  } while ((opcion > 4) || (opcion < 0));
  system("@cls||clear");
  return opcion;
}

void buildMatrix(int matrix[10][10], int *row, int *column)
{
  int i, j;

  printf("CARGA MATRIZ\n");

  printf("\nCantidad de filas: ");
  scanf("%d", row);

  printf("\nCantidad de columnas: ");
  scanf("%d", column);

  for (i = 0; i < (*row); i++)
  {
    for (j = 0; j < (*column); j++)
    {
      int aux = 0;
      printf("Elemento [%d][%d]:", i, j);
      scanf("%d", &aux);
      matrix[i][j] = aux;
    }
    printf("\n");
  }

  system("@cls||clear");
};

void showMatrix(int matrix[10][10], int row, int column)
{
  int i, j;

  printf("\nELEMENTOS DE LA MATRIZ\n");

  for (i = 0; i < row; i++)
  {
    for (j = 0; j < column; j++)
    {
      printf("[%d] ", matrix[i][j]);
    }
    printf("\n");
  }
};

void showMainDiagonal(int matrix[10][10], int row, int column)
{
  int i, j;

  printf("\nELEMENTOS DE LA DIAGONAL PRINCIPAL: ");

  for (i = 0; i < row; i++)
  {
    for (j = 0; j < column; j++)
    {
      if (i == j)
      {
        printf("[%d] ", matrix[i][j]);
      }
    }
  }
};

void showSecondDiagonal(int matrix[10][10], int row, int column)
{
  int i, j;

  printf("\nELEMENTOS DE LA DIAGONAL SECUNDARIA: ");

  for (i = 0; i < row; i++)
  {
    for (j = 0; j < column; j++)
    {
      if (j + 1 == (column - i))
        printf("[%d] ", matrix[i][j]);
    }
  }
};
