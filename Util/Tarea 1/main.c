#include <stdio.h>
#include <stdlib.h>

#define FILENAME "quote.txt"

void openQuote();

int main() {
  
  openQuote();

  return 0;
}

void openQuote () {
  FILE *file = fopen(FILENAME, "r");

  if(!file) {
    printf("No se pudo abrir el archivo");
    return;
  }

  
  while (!feof(file)){
    char firstName[30], lastName[30];
    int edad, id;

    fscanf(file, "%[^ ] %[^|]|%d|%d\n", firstName, lastName, &edad, &id);
    printf("%d,%d,%s\n", id, edad, lastName);
  }
  

  
  
  fclose(file);
}
