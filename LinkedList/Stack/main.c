#include <stdlib.h>
#include <stdio.h>
#include "stack.h"

int main(){
  Stack stack;
  stack.head = NULL;

  push(&stack, 1);
  push(&stack, 2);
  push(&stack, 3);
  push(&stack, 4);

  show(&stack);
  printf("\nValor on top: %d\n", peek(stack));

  printf("\nValor eliminado: %d\n", pop(&stack));

  printf("\nValor on top: %d\n", peek(stack));

  show(&stack);

  printf("\nLISTA VACIA: %s\n", isEmpty(stack) ? "SI" : "NO");


  return 0;
}
