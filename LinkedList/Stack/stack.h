typedef struct node {
  int value;
  struct node *next;
} Node;

typedef struct {
  struct node *head;
}Stack;


void push (Stack *stack, int value);
void show(Stack *stack);
int pop (Stack *stack);
int peek (Stack stack);
int isEmpty (Stack stack);

