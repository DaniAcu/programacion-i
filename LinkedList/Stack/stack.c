#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "stack.h"

void push (Stack *stack, int value){
  Node *newNode = malloc(sizeof(Node));
  newNode->value = value;
  newNode->next = stack->head;
  stack->head = newNode;
}

void show(Stack *stack){
  if(isEmpty(*stack)) return;

  int poped = pop(stack);
  printf(" %d -->", poped);
  show(stack);
  push(stack, poped);
}

int pop (Stack *stack){
  if(isEmpty(*stack)) return INT_MIN;

  Node *deleted;
  deleted = stack->head;
  stack->head = stack->head->next;
  int value = deleted->value;

  free(deleted);

  return value;
}

int peek (Stack stack){
  if (isEmpty(stack)) return INT_MIN;

  return stack.head->value;  
}

int isEmpty (Stack stack){
  return stack.head == NULL;
}
