
#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "tree.h"

int main() {
  List list = initList();
  loadTemperature(&list);
  printf("\n");
  showList(list);
  printf("\n");
  float rootValue = getMiddleNode(list);

  printf("Root: %f\n", rootValue);

  TreeNode *root = createTreeNode(rootValue);

  Node *currentNode = list.head;

  while(currentNode != NULL) {
    addNodeToTree(&root, currentNode->value);
    currentNode = currentNode->next;
  }

  printf("\n");
  inorder(root);
  printf("\n");
  printf("Altura del arbol: %d", getTreeHigh(root));

  printf("\n");

  return 0;
}

