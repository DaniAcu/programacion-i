typedef struct node {
  float value;
  struct node *next;
  struct node *prev;
} Node;


typedef struct list {
  struct node *head;
  struct node *tail;
} List;

Node *createNode (float value);
List initList();
void addNode(List *list, float value);
void showList(List list);
float getMiddleNode(List list);
void loadTemperature(List *list);
