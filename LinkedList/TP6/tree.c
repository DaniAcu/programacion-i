#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

TreeNode *createTreeNode (float value){
    TreeNode *node = malloc(sizeof(TreeNode));
    node->left = NULL;
    node->right = NULL;
    node->value = value;

    return node;
}

void addNodeToTree(TreeNode **node, float value){
    if(*node == NULL) {
        *node = createTreeNode(value);
        return;
    }

    if((*node)->value > value) {
        addNodeToTree(&(*node)->left, value);
    }else if((*node)->value < value) {
        addNodeToTree(&(*node)->right, value);
    }
}

void inorder(TreeNode *root) { 
    if (root != NULL) { 
        inorder(root->left);
        printf("%.2f ", root->value);
        inorder(root->right);
    } 
}

int getTreeHigh(TreeNode *root) {
    if (root == NULL) return 0;

    int leftHigh = getTreeHigh(root->left);
    int rightHigh = getTreeHigh(root->right); // <--------
    return 1 + (leftHigh > rightHigh ? leftHigh : rightHigh);
}
