#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "list.h"
#define FILENAME "temperaturas.txt"

Node *createNode (float value) {
    Node *node = malloc(sizeof(Node));
    node->value = value;
    node->next = NULL;
    node->prev = NULL;

    return node;
}

List initList () {
    List list;
    list.head = NULL;
    list.tail = NULL;

    return list;
}

void addNode(List *list, float value) {
    Node *newNode = createNode(value);
    
    if(list->head == NULL && list->tail == NULL) {
        list->head = newNode;
        list->tail = newNode;
    } else {
        newNode->next = list->head;
        list->head->prev = newNode;
        list->head = newNode;
    }
}

void showList(List list) {
    if(list.head == NULL || list.tail == NULL) return;

    Node *node = list.head;
    while (node != NULL) {
        printf(" %.2f -->", node->value);
        node = node->next;
    };
}

float getMiddleNode(List list) {
    float average = 0;
    float total = 0;
    int size = 0;

    Node *currentNode = list.head;
    while (currentNode != NULL) {
        size++;
        total += currentNode->value;
        currentNode = currentNode->next;
    };

    average = total / size;

    float minDiff = fabs(list.head->value - average);
    float middleValue = list.head->value;

    currentNode = list.head->next;
    while (currentNode != NULL) {
        float distance = fabs(currentNode->value - average);
        if(minDiff >= distance) {
            minDiff = distance;
            middleValue = currentNode->value;
        }
        currentNode = currentNode->next;
    };

    return middleValue;
}



void loadTemperature (List *list) {
    FILE *file = fopen(FILENAME, "r");

    if(file == NULL) {
        printf("Can't open the file");
        return;
    }

    while (!feof(file)) {
        float value;
        fscanf(file, "%f\n", &value);
        addNode(list, value);
    }

    fclose(file);
    printf("Temperaturas cargadas correctamente!");
}
