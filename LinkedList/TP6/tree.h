typedef struct treenode {
  float value;
  struct treenode *right;
  struct treenode *left;
} TreeNode;

TreeNode *createTreeNode (float value);
void addNodeToTree(TreeNode **tree, float value);
void inorder(TreeNode *root);
int getTreeHigh(TreeNode *root);
