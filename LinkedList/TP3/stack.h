typedef struct stacknode {
  int value;
  struct stacknode *next;
} StackNode;

typedef struct {
  struct stacknode *head;
}Stack;


void push (Stack *stack, int value);
void showStack(Stack *stack);
int pop (Stack *stack);
int isStackEmpty (Stack stack);
Stack genereateA();
Stack genereateB();
Stack genereateC(Stack *A, Stack *B);
void merge(Stack *A, Stack *B, Stack *C);
void search (Stack *stack, int a, int *matches);

