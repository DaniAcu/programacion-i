typedef struct queuenode {
  int value;
  struct queuenode *next;
} QueueNode;

typedef struct {
  struct queuenode *head;
  struct queuenode *tail;
}Queue;


Queue generateQ();
void enqueue(Queue *queue, int value);
int dequeue(Queue *queue);
int isQueueEmpty(Queue queue);
void showQueue(Queue *queue);
void searchInQueue (Queue *queue, int a, int *matches);
