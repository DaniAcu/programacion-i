#include <stdlib.h>
#include <stdio.h>
#include "circle.h"

int main(){
  Stack A = genereateA();
  
  printf("Ejercicio 1:\n");
  printf("Dada dos pilas A y B, generar una pila C que tenga los elementos comunes entre A y B (generar una pila que sea la intersección de las dos anteriores).\n");

  Stack B = genereateB();
  Stack C = genereateC(&A, &B);

  printf("Pila A: "); showStack(&A); printf("\n");
  printf("Pila B: "); showStack(&B); printf("\n");
  printf("Pila C: "); showStack(&C); printf("\n");

  printf("\n");
  printf("Ejercicio 2:\n");
  printf("a partir de una pila y una cola, agregar en una lista circular los elementos que tienen en común.\n");

  Queue Q = generateQ();
  Circle list = generateCircleList(&A, &Q);

  printf("Pila A: "); showStack(&A); printf("\n");
  printf("Queue: "); showQueue(&Q); printf("\n");
  printf("Circle List: "); showList(list); printf("\n");

  printf("\n");
  printf("Ejercicio 3:\n");
  printf("Dada una pila y una cola, sacar un elemento de la pila y verificar si esta en la cola.\n");

  printf("Pila A: "); showStack(&A); printf("\n");
  printf("Queue: "); showQueue(&Q); printf("\n");
  int item = pop(&A);
  printf("Elemento de A: %d\n", item);
  int machesOnQ = 0;
  searchInQueue(&Q, item, &machesOnQ);
  printf("¿Está en la cola?: %s\n", machesOnQ > 0 ? "SI" : "NO");
  printf("Pila A despues: "); showStack(&A); printf("\n");



  return 0;
}
