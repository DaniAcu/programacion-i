#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "stack.h"

void push (Stack *stack, int value){
  StackNode *newNode = malloc(sizeof(StackNode));
  newNode->value = value;
  newNode->next = stack->head;
  stack->head = newNode;
}

void showStack(Stack *stack){
  if(isStackEmpty(*stack)) return;

  int poped = pop(stack);
  printf(" %d -->", poped);
  showStack(stack);
  push(stack, poped);
}

int pop (Stack *stack){
  if(isStackEmpty(*stack)) return INT_MIN;

  StackNode *deleted;
  deleted = stack->head;
  stack->head = stack->head->next;
  int value = deleted->value;

  free(deleted);

  return value;
}

int isStackEmpty (Stack stack){
  return stack.head == NULL;
}

Stack genereateA() {
  Stack stack;
  stack.head = NULL;

  push(&stack, 1);
  push(&stack, 2);
  push(&stack, 3);
  push(&stack, 4);

  return stack;
}

Stack genereateB() {
  Stack stack;
  stack.head = NULL;

  push(&stack, 4);
  push(&stack, 5);
  push(&stack, 6);
  push(&stack, 7);
  push(&stack, 8);

  return stack;
}

void search (Stack *stack, int a, int *matches) {
  if(isStackEmpty(*stack)) return;

  int poped = pop(stack);
  if(a == poped) *matches +=1;
  search(stack, a, matches);
  push(stack, poped);
}

void merge(Stack *A, Stack *B, Stack *C){
  if(isStackEmpty(*A)) return;

  int a = pop(A);
  int matchesB = 0;
  search(B, a, &matchesB);

  if(matchesB > 0) push(C, a);

  merge(A,B,C);

  push(A, a);
}

Stack genereateC(Stack *A, Stack *B) {
  Stack stack;
  stack.head = NULL;

  merge(A, B, &stack);
  
  return stack;
}

