#include <stdlib.h>
#include <stdio.h>
#include "circle.h"

void addCircle(Circle *list, int value) {
  CircleNode *newNode = malloc(sizeof(CircleNode));
  newNode->value = value;
  newNode->next = NULL;

  if(list->first == NULL && list->last == NULL) {
    list->first = newNode;
    list->last = newNode;
    newNode->next = newNode->prev = newNode;
  } else {
    newNode->next = list->first;
    newNode->prev = list->last;
    list->last->next = newNode;
    list->first->prev = newNode;
    list->first = newNode;
  }
}

void mergeIntoACircle(Stack *A, Queue *B, Circle *C){
  if(isStackEmpty(*A)) return;

  int a = pop(A);
  int matchesB = 0;
  searchInQueue(B, a, &matchesB);

  if(matchesB > 0) addCircle(C, a);

  mergeIntoACircle(A,B,C);

  push(A, a);
}

Circle generateCircleList(Stack *stack, Queue *queue) {
  Circle list;
  list.first = NULL;
  list.last = NULL;

  mergeIntoACircle(stack, queue, &list);

  return list;
}

void showList(Circle list) {
    if(list.first == NULL || list.last == NULL) return;
    CircleNode *node = list.first;
    do {
        printf("%d -->", node->value);
        node = node->next;
    } while (node != list.first);
}
