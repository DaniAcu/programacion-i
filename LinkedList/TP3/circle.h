#include "stack.h"
#include "queue.h"

typedef struct circlenode {
  int value;
  struct circlenode *next;
  struct circlenode *prev;
} CircleNode;

typedef struct {
  struct circlenode *first;
  struct circlenode *last;
} Circle;


void addCircle(Circle *list, int value);
void mergeIntoACircle(Stack* stack, Queue *queue, Circle *list);
Circle generateCircleList(Stack *stack, Queue *queue);
void showList(Circle list);


