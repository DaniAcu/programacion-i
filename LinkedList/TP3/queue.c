#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "queue.h"

Queue generateQ(){
  Queue queue;
  queue.head = NULL;
  queue.tail = NULL;

  enqueue(&queue, 3);
  enqueue(&queue, 4);
  enqueue(&queue, 5);
  enqueue(&queue, 6);
  enqueue(&queue, 7);
  
  return queue;
}

void enqueue(Queue *queue, int value){
  QueueNode *newNode = malloc(sizeof(QueueNode));
  newNode->value = value;
  newNode->next = NULL;

  if(isQueueEmpty(*queue)) {
    queue->head = newNode;
  } else {
    queue->tail->next = newNode;
  }

  queue->tail = newNode;  
}

int dequeue(Queue *queue){
  if(isQueueEmpty(*queue)) return INT_MIN;
  QueueNode *deleted = queue->head;
  
  if(queue->head == queue->tail) queue->tail = NULL;
  queue->head = queue->head->next;



  int value = deleted->value;

  free(deleted);

  return value;
}

int isQueueEmpty(Queue queue){
  return queue.head == NULL && queue.tail == NULL;
}

void showQueue(Queue *queue){
  if(isQueueEmpty(*queue)) return;

  int value = dequeue(queue);
  printf("%d --> ", value);
  showQueue(queue);
  enqueue(queue, value);
}

void searchInQueue (Queue *queue, int a, int *matches) {
  if(isQueueEmpty(*queue)) return;

  int poped = dequeue(queue);
  if(a == poped) *matches +=1;
  searchInQueue(queue, a, matches);
  enqueue(queue, poped);
}
