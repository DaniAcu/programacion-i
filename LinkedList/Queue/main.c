#include <stdlib.h>
#include <stdio.h>
#include "queue.h"

int main(){
  Queue queue = createEmptyQueue();

  enqueue(&queue, 1);
  enqueue(&queue, 2);
  enqueue(&queue, 3);
  enqueue(&queue, 4);

  show(&queue);
  
  printf("\nValor on top: %d\n", peek(queue));

  printf("\nValor eliminado: %d\n", dequeue(&queue));

  printf("\nValor on top: %d\n", peek(queue));

  show(&queue);

  printf("\nLISTA VACIA: %s\n", isEmpty(queue) ? "SI" : "NO");


  return 0;
}
