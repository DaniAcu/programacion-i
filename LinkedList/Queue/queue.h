typedef struct node {
  int value;
  struct node *next;
} Node;

typedef struct {
  struct node *head;
  struct node *tail;
}Queue;


Queue createEmptyQueue();
void enqueue(Queue *queue, int value);
int dequeue(Queue *queue);
int peek(Queue queue);
int isEmpty(Queue queue);
void show(Queue *queue);
