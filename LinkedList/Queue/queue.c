#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "queue.h"

Queue createEmptyQueue(){
  Queue queue;
  queue.head = NULL;
  queue.tail = NULL;

  return queue;
}

void enqueue(Queue *queue, int value){
  Node *newNode = malloc(sizeof(Node));
  newNode->value = value;
  newNode->next = NULL;

  if(isEmpty(*queue)) {
    queue->head = newNode;
  } else {
    queue->tail->next = newNode;
  }

  queue->tail = newNode;  
}

int dequeue(Queue *queue){
  if(isEmpty(*queue)) return INT_MIN;
  Node *deleted = queue->head;
  
  if(queue->head == queue->tail) queue->tail = NULL;
  queue->head = queue->head->next;



  int value = deleted->value;

  free(deleted);

  return value;
}

int peek(Queue queue){
  if(queue.head == NULL) return INT_MIN;

  return queue.head->value;
}

int isEmpty(Queue queue){
  return queue.head == NULL && queue.tail == NULL;
}

void show(Queue *queue){

  Queue reversedQueue = createEmptyQueue();
  while(!isEmpty(*queue)){
    int value = dequeue(queue);
    printf("%d --> ", value);
    enqueue(&reversedQueue, value);
  }

  // Go back to original order
  while(!isEmpty(reversedQueue)){
    int value = dequeue(&reversedQueue);
    enqueue(queue, value);
  }
}
