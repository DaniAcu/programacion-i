#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "file.h"
#define EMPLOYERS_FILE "EMPLEADOS.csv"
#define SALES_FILE "VENTAS.csv"
#define MAX_LENGTH 50 


// Fill list with files

void loadSales(SaleNode **saleList, int employerId) {
    FILE *salesFile = openFile(SALES_FILE, "r");

    if (salesFile != NULL) {
        int id, month, totalSales;

        while (feof(salesFile) == 0)
        {
            fscanf(salesFile, "%d,%d,%d\n", &id, &month, &totalSales);
            if(id == employerId) {
                Sale sale;
                sale.month = month;
                sale.totalAmount = totalSales;
                addSaleNodeInOrder(saleList, sale);
            }
        }
        fclose(salesFile);
    }
}

void loadEmployers(EmployerNode **employerList) {
    FILE *employersFile = openFile(EMPLOYERS_FILE, "r");

    if (employersFile != NULL) {
        int id, laborYears;
        char firstName[MAX_LENGTH], lastName[MAX_LENGTH], category;

        while (feof(employersFile) == 0) {
            fscanf(employersFile, "%d,%[^,],%[^,],%d,%c\n", &id, firstName, lastName, &laborYears, &category);
            
            Employer employer;
            employer.id = id;
            strcpy(employer.firstName, firstName);
            strcpy(employer.lastName, lastName);
            employer.laborYears = laborYears;
            employer.category = category;
            employer.sales = NULL;
            loadSales(&employer.sales, employer.id);
            addEmployerNodeInOrder(employerList, employer);
        }
        fclose(employersFile);
    }
}

// Manage List and SubList

EmployerNode *createEmployerNode(Employer employer) {
    EmployerNode *node = malloc(sizeof(EmployerNode));
    node->employer = employer;
    node->next = NULL;

    return node;
}

SaleNode *createSaleNode(Sale sale) {
    SaleNode *node = malloc(sizeof(SaleNode));
    node->sale = sale;
    node->next = NULL;

    return node;
}

void addEmployerNodeInOrder(EmployerNode **head, Employer employer) {
    EmployerNode *newNode = createEmployerNode(employer);
    EmployerNode *iNode = *head;

    while (iNode != NULL && iNode->next != NULL && iNode->next->employer.id < employer.id){
        iNode = iNode->next;
    }

    if(*head == NULL) {
        *head = newNode;
    }else {
        newNode->next = iNode->next;
        iNode->next = newNode;
    }
}

void addSaleNodeInOrder(SaleNode **head, Sale sale) {
    SaleNode *newNode = createSaleNode(sale);
    SaleNode *iNode = *head;

    while (iNode != NULL && iNode->next != NULL && iNode->next->sale.month < sale.month){
        iNode = iNode->next;
    }

    if(*head == NULL) {
        *head = newNode;
    }else {
        newNode->next = iNode->next;
        iNode->next = newNode;
    }
}

// Report

void generateReport(EmployerNode *head) {
    float total1Semester = 0, totalAnnual = 0;
    printfLineWithChar("=");
    consoleCenterLine("REPORTES DE SUELDOS COMPLEMENTARIOS", 120);
    printfLineWithChar("=");
    consoleLine("Nombres y Apellidos", 60);
    consoleLine("Sueldo acumulado 1er semestre", 30);
    consoleLine("Sueldo anual", 30);
    printfLineWithChar("=");
    
    EmployerNode *node = head;
    while (node != NULL){
        float employerSemesterSalary = 0, employerYearCompSalary = 0;
        consoleLine(getFullName(node->employer), 60);
        for (int i = 1; i <= 6; i++) {
            float monthSalary = getSalary(node->employer, 1);
            if(employerYearCompSalary < monthSalary) employerYearCompSalary = monthSalary;
            employerSemesterSalary += monthSalary;
        }
        employerYearCompSalary = employerYearCompSalary / 2;
        consoleCenterLine(formatPrice(employerSemesterSalary), 30);
        consoleCenterLine(formatPrice(employerYearCompSalary), 30);

        //consoleLine(getFullName(node->employer), 60);
        printf("\n");
        total1Semester += employerSemesterSalary;
        totalAnnual += employerYearCompSalary;
        node = node->next;
    }
    printfLineWithChar("-");
    consoleLine("Totales", 60);
    consoleCenterLine(formatPrice(total1Semester), 30);
    consoleCenterLine(formatPrice(totalAnnual), 30);
    printfLineWithChar("-");
    
    consoleLine("Vendedor con mayor importe de ventas", 90);
    consoleLine(getFullName(getBestEmployer(head)), 30);

    printfLineWithChar("=");
    
}

// Employers Utils

float getSalary (Employer employer, int month) {
    float salary = 0;

    // Category    
    if (employer.category == 'A' ) salary += 9000;
    if (employer.category == 'B' ) salary += 15000;
    if (employer.category == 'C' ) salary += 20000;
    if (employer.category == 'D' ) salary += 25000;


    // Antiguedad
    salary += ((salary / 100) * 2) * employer.laborYears;
    

    SaleNode *node = employer.sales;
    while (node != NULL && month != node->sale.month){
        node = node->next;
    }

    int salesPorcentage = salary > 10000 ? 10 : 5;

    if(node == NULL) return 0;

    salary += (node->sale.totalAmount / 100.0) * salesPorcentage;

    return salary;
}

Employer getBestEmployer (EmployerNode *head) {
    EmployerNode *node = head;
    EmployerNode *bestEmployer = NULL;
    int bestEmployerSalesAmount = 0;
    while (node != NULL){
        int totalSalesByEmployer = 0;
        
        SaleNode *saleNode = node->employer.sales;
        while (saleNode != NULL){
            totalSalesByEmployer += saleNode->sale.totalAmount;
            saleNode = saleNode->next;
        }

        if(bestEmployerSalesAmount < totalSalesByEmployer) {
            bestEmployerSalesAmount = totalSalesByEmployer;
            bestEmployer = node;
        }

        node = node->next;
    }

    return bestEmployer->employer;
}

char *getFullName (Employer employer) {
    int length = strlen(employer.firstName) + strlen(employer.lastName) + 1;
    char *fullName = malloc(sizeof(char) * length);
    
    sprintf(fullName, "%s %s", employer.firstName, employer.lastName);

    return fullName;
}

char *formatPrice (float number) {
    int n = (int) number, 
        // length all time will have 2 float number, 1 char for dot and 1 for the simbol
        length = 4;
    
    // Counting digits
    while(n != 0){  
       n = n / 10;  
       length++;
    }

    char *price = malloc(sizeof(char) * length);
    sprintf(price, "$%.2f", number);

    return price;
}

// Console uitls

void printfLineWithChar(char *s) {
    printf("\n");
    for (int i = 0; i < 120; i++) printf(s);
    printf("\n");
}

void consoleCenterLine(char *s, int space) {
    int length = (space - strlen(s)) / 2;
    for (int i = 0; i < length; i++) printf(" ");
    printf(s);
    for (int i = 0; i < length; i++) printf(" ");
}

void consoleLine(char *s, int space) {
    int length = space - strlen(s) - 4;
    for (int i = 0; i < 4; i++) printf(" ");
    printf(s);
    for (int i = 0; i < length; i++) printf(" ");
}
