
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int main() {
  EmployerNode *head = NULL;
  loadEmployers(&head);

  generateReport(head);

  return 0;
}

