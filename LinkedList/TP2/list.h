#define MAX_LENGTH 50 

typedef struct {
  int month;
  int totalAmount;
} Sale;

typedef struct SaleNode {
  Sale sale;
  struct SaleNode *next; 
} SaleNode;

typedef struct {
  int id;
  char firstName[MAX_LENGTH];
  char lastName[MAX_LENGTH];
  int laborYears;
  char category;
  SaleNode *sales;
} Employer;

typedef struct EmployerNode {
  Employer employer;
  struct EmployerNode *next; 
} EmployerNode;

EmployerNode *createEmployerNode(Employer employer);
SaleNode *createSaleNode(Sale sale);
void addEmployerNodeInOrder(EmployerNode **head, Employer employer);
void addSaleNodeInOrder(SaleNode **head, Sale sale);
void loadSales(SaleNode **saleList, int employerId);
void loadEmployers(EmployerNode **employerList);
float getSalary (Employer employer, int month);
Employer getBestEmployer (EmployerNode *head);
void printfLineWithChar(char *s);
void generateReport(EmployerNode *head);
void consoleLine(char *s, int space);
void consoleCenterLine(char *s, int space);
char *getFullName (Employer employer);
char *formatPrice (float number);
