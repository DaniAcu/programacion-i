typedef struct node {
  void *value;
  struct node *next; 
} Node;

typedef struct {
  struct node *head;
} List;


Node *createNode (void *value);
List initList();
void addNode(List *list, void *value);
void addNodeInOrder(List *list, void *value, int (*sortMatch)(Node *, Node*));
void sortList(List *list, int (*match)(Node *, Node*));
void filterList(List *list, int (*checkValue)(void *));
void deleteList(List *list);
void showList(List list, void (*function)(void *));