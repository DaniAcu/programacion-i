#include <stdio.h>
#include <stdlib.h>
#include "list.h"

Node *createNode (void *value) {
    Node *pNode = malloc(sizeof(Node));
    pNode->value = value;
    pNode->next = NULL;

    return pNode;
}

List initList () {
    List list;
    list.head = NULL;

    return list;
}

void addNode(List *list, void *value) {
    Node *newNode = createNode(value);
    newNode->next = list->head;
    list->head = newNode;
}

void addNodeInOrder(List *list, void *value, int (*sortMatch)(Node *, Node*)) {
    addNode(list, value);
    sortList(list, sortMatch);
}

void filterList(List *list, int (*checkValue)(void *)) {

    if(checkValue(list->head->value)){
        Node *eliminado = list->head;
        list->head = list->head->next;
        free(eliminado);
    }

    Node *iNode = list->head;
    while (iNode->next != NULL){  
        if((*checkValue)(iNode->next->value)){
            Node *eliminado = iNode->next;
            iNode->next = iNode->next->next;
            free(eliminado);
        }
        
        iNode = iNode->next;
    }
}

void deleteList(List *list) {
    Node *deleted;
    while (list->head != NULL){
       deleted = list->head;
       list->head = list->head->next;
       free(deleted);
    }
}

void sortList(List *list, int (*match)(Node *, Node*)) {
    int isOrdered = 0;

    if(list->head == NULL || list->head->next == NULL) return;

    while (isOrdered == 0){
        isOrdered = 1;
        Node *node = list->head;
        while (node->next != NULL){
            if(match(node, node->next)){
                isOrdered = 0;
                void *temp = node->value;
                node->value = node->next->value;
                node->next->value = temp;
            }
            node = node->next;
        }
    } 
}

void showList(List list, void (*callback)(void *)) {
    printf("\n");
    Node *node = list.head;
    while (node != NULL){
        callback(node->value);
        node = node->next;
    }
}
