
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

void showIntNode (void *data) {
  printf("%c --> ", *(char *)data);
}

int isA(void *data){
  return *(char *)data == 'a';
}

int sortLarger (Node *prevNode, Node *nextNode) {
  return prevNode->value > nextNode->value;
}

int sortShorter (Node *prevNode, Node *nextNode) {
  return prevNode->value < nextNode->value;
}


int main() {
  char a = 'a', b = 'b', c = 'c', d = 'd';
  List list = initList();
  addNode(&list, &b);
  addNode(&list, &a);
  addNode(&list, &c);
  addNode(&list, &a);

  // Inital List
  showList(list, showIntNode);

  // Add duplicated Node

  filterList(&list, isA);
  // Show filtred list
  showList(list, showIntNode);

  // RESTORE INTIAL LIST
  addNode(&list, &a);

  // Sort form MAX to MIN
  sortList(&list, sortLarger);

  showList(list, showIntNode);

  // Sort form MIN to MAX
  sortList(&list, sortShorter);

  showList(list, showIntNode);

  // Add new node following the order
  addNodeInOrder(&list, &d, sortShorter);

  showList(list, showIntNode);
  
  return 0;
}

