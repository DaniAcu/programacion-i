typedef struct node{
  int value;
  struct node *next;
} Node;

typedef struct list{
  struct node *head;
} List;

Node *generateNode(int value);
void addNode (List *list, int value);
void showList(List list);
void showListR(Node *node);


