
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int main() {
  printf("Simple List\n");

  List list;
  list.head = NULL;

  addNode(&list, 2);
  addNode(&list, 1);
  addNode(&list, 3);

  showListR(list.head);

  return 0;
}

