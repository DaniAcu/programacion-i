#include <stdlib.h>
#include <stdio.h>
#include "list.h"

Node *generateNode(int value) {
  Node *node = malloc(sizeof(Node));
  node->value = value;
  node->next = NULL;

  return node;
}
void addNode (List *list, int value) {
  Node *newNode = generateNode(value);

  newNode->next = list->head;
  list->head = newNode;
}
void showList(List list) {
  Node *current = list.head;

  while (current != NULL){
    printf("%d --> ", current->value);
    current = current->next;
  }  
}

void showListR(Node *node) {
  Node *current = node;
  if(current != NULL) {
    printf("%d --> ", current->value);
    showListR(current->next);
  }

}