typedef struct  {
  int id;
  int idLot;
  char name[25];
  int amount;
  int date[3];
} Vaccination;

typedef struct node {
  Vaccination value;
  struct node *next;
} Node;

typedef struct {
  struct node *head;
  struct node *tail;
} Queue;

typedef struct treenode {
    struct treenode *right;
    struct treenode *left;
    Vaccination value;
} TreeNode;

Queue createEmptyQueue();
void enqueue(Queue *queue, Vaccination value);
void dequeue(Queue *queue);
Vaccination peek(Queue queue);
int isEmpty(Queue queue);
void show(Queue *queue);

TreeNode *generateTreeNode(Vaccination value);
void insertar(TreeNode **node, Vaccination value);
void inorder(TreeNode *root);
