#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "queue.h"

Queue createEmptyQueue(){
  Queue queue;
  queue.head = NULL;
  queue.tail = NULL;

  return queue;
}

void enqueue(Queue *queue, Vaccination value){
  Node *newNode = malloc(sizeof(Node));
  newNode->value = value;
  newNode->next = NULL;

  if(isEmpty(*queue)) {
    queue->head = newNode;
  } else {
    queue->tail->next = newNode;
  }

  queue->tail = newNode;  
}

void dequeue(Queue *queue){
  if(isEmpty(*queue)) return INT_MIN;
  Node *deleted = queue->head;
  
  if(queue->head == queue->tail) queue->tail = NULL;
  queue->head = queue->head->next;

  free(deleted);
}

Vaccination peek(Queue queue){
  return queue.head->value;
}

int isEmpty(Queue queue){
  return queue.head == NULL && queue.tail == NULL;
}

TreeNode *generateTreeNode(Vaccination value){
    TreeNode *node = malloc(sizeof(TreeNode));
    node->left = NULL;
    node->right = NULL;
    node->value = value;

    return node;
}

void insertar(TreeNode **node, Vaccination value){
    if(*node == NULL) {
        *node = generateTreeNode(value);
    }

    if((*node)->value.id > value.id){
        insertar(&(*node)->left, value);
    } else if ((*node)->value.id < value.id){
        insertar(&(*node)->right, value);
    } 
}



void inorder(TreeNode *root) { 
    if (root == NULL) {
        return;
    }

    inorder(root->left);
    printf("%d ", root->value);
    inorder(root->right);

}

void print2DUtil(TreeNode *root, int space) { 
    // Base case 
    if (root == NULL) 
        return; 
  
    // Increase distance between levels 
    space += 10; 
  
    // Process right child first 
    print2DUtil(root->right, space); 
  
    // Print current node after space 
    // count 
    printf("\n"); 
    for (int i = 10; i < space; i++) printf(" "); 
    printf("%d\n", root->value); 
  
    // Process left child 
    print2DUtil(root->left, space); 
} 

