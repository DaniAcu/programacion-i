typedef struct userNode{
  int id;
  struct userNode *next;
} UserNode;

typedef struct userList{
  struct userNode *head;
} UserList;

typedef struct cursoNode{
  int aula;
  struct cursoNode *next;
  struct userList users;
} CursoNode;

typedef struct list{
  struct cursoNode *head;
} CursoList;

UserNode *generateUserNode(int id);
CursoNode *generateCursoNode(int aula);
void addUserNode (UserList *list, int id);
void addCursoNode (CursoList *list, int aula);
void showList(CursoList list);
