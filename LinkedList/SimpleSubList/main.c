
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int main() {
  printf("Simple List\n");

  CursoList list;
  list.head = NULL;

  addCursoNode(&list, 1);
  addUserNode(&(list.head->users), 1);
  addUserNode(&(list.head->users), 2);
  addUserNode(&(list.head->users), 3);
  addCursoNode(&list, 2);
  addUserNode(&(list.head->users), 1);
  addUserNode(&(list.head->users), 2);
  addUserNode(&(list.head->users), 3);
  

  showList(list);

  return 0;
}

