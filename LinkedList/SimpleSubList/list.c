#include <stdlib.h>
#include <stdio.h>
#include "list.h"

UserNode *generateUserNode(int id){
  UserNode *node = malloc(sizeof(UserNode));
  node->id = id;
  node->next = NULL;

  return node;
}

CursoNode *generateCursoNode(int aula){
  CursoNode *node = malloc(sizeof(CursoNode));
  node->aula = aula;
  node->next = NULL;
  node->users.head = NULL;

  return node;
}

void addUserNode (UserList *list, int id){
  UserNode *newNode = generateUserNode(id);

  newNode->next = list->head;
  list->head = newNode;
}

void addCursoNode (CursoList *list, int aula){
  CursoNode *newNode = generateCursoNode(aula);

  newNode->next = list->head;
  list->head = newNode;
}

void showList(CursoList list){
  CursoNode *currentCurso = list.head;

  while (currentCurso != NULL){
    printf("Curso %d\n", currentCurso->aula);
    UserNode *currentUser = currentCurso->users.head;
    printf("Users ");
    while (currentUser != NULL){
      printf("%d --> ", currentUser->id);
      currentUser = currentUser->next;
    }
    printf("\n\n");
    currentCurso = currentCurso->next;
  }
  /*
    Curso 1:
    Users 1 --> 2 --> 3--->

    Curso 2:
    Users 1 --> 2 --> 3--->
  */  
}
