#include "user.h"

typedef struct node {
  User value;
  struct node *next;
  struct node *prev;
} Node;

typedef struct list {
  Node *first;
  Node *last;
} List;


Node *createNode (User value);
List *createList();
void addNode(List *list, User value);
void fillList (List *list);
void addNodeInOrder(List *list, User value);
void orderListByValue(List *list);
void showList(List list);
void showListInverse(List list);
void renameUser(List *list, char name[20], char newName[20]);
void removeUser(List *list, char name[20]);
void deleteList(List *list);
List *getAdultList(List *list);
