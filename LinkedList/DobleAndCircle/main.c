
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int main() {
  List *list = createList();
  fillList(list);

  showList(*list);

  renameUser(list, "Abel Acuña", "Nahuel Roldan");

  showList(*list);

  List *adultList = getAdultList(list);

  showList(*adultList);

  removeUser(list, "Nahuel Roldan");

  showList(*list);

  deleteList(list);

  showList(*list);
  

  return 0;
}

