#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

#define FILENAME_CSV "users.csv"

Node *createNode (User value) {
    Node *pNode = malloc(sizeof(Node));
    pNode->value = value;
    pNode->next = NULL;

    return pNode;
}

List *createList () {
    List *list = malloc(sizeof(List));
    list->first = NULL;
    list->last = NULL;

    return list;
}

// add delete all;

void addNode(List *list, User value) {
    Node *newNode = createNode(value);
    
    if(list->first == NULL && list->last == NULL) {
        list->first = newNode;
        list->last = newNode;
        newNode->next = newNode->prev = newNode;
    } else {
        newNode->next = list->first;
        newNode->prev = list->last;
        list->last->next = newNode;
        list->first->prev = newNode;
        list->first = newNode;
    }
}

void addNodeInOrder(List *list, User value) {
    Node *newNode = createNode(value);
    
    if(list->first == NULL && list->last == NULL) {
        list->first = newNode;
        list->last = newNode;
        newNode->next = newNode->prev = newNode;
        return;
    }

    Node *currentNode = list->first;

    while (currentNode != list->last && currentNode->value.years > newNode->value.years){
        currentNode = currentNode->next;
    };


    if (currentNode == list->last && currentNode->value.years > newNode->value.years) {
        newNode->prev = list->last;
        newNode->next = list->first;
        list->last->next = list->first->prev = newNode;
        list->last = newNode;
    } else if(currentNode == list->first) {
        newNode->prev = list->last;
        newNode->next = list->first;
        list->last->next = list->first->prev = newNode;
        list->first = newNode;
    } else {
        newNode->next = currentNode;
        newNode->prev = currentNode->prev;
        currentNode->prev = newNode;
        newNode->prev->next = newNode;
    }
}

void orderListByValue(List *list) {
    int isOrdered = 0;

    if(list->first == NULL || list->last == NULL) return;

    while (isOrdered == 0){
        isOrdered = 1;
        Node *node = list->first;
        while (node != list->last){
            if(node->value.years > node->next->value.years){
                isOrdered = 0;
                User temp = node->value;
                node->value = node->next->value;
                node->next->value = temp;
            }
            node = node->next;
        }
    } 
}

void fillList (List *list) {
  FILE *usersFileCSV = openFile(FILENAME_CSV, "r");
    
  if(usersFileCSV != NULL){
    User user;
    char firstName[20] = "", lastName[20] = "";
    int day, month, year;

    
    // Daniel,Acuña,19980831

    while(feof(usersFileCSV) == 0){
        fscanf(usersFileCSV, "%[^,],%[^,],%d/%d/%d\n", firstName, lastName, &day, &month, &year);
        strcpy(user.name, firstName);
        strcat(user.name, " ");
        strcat(user.name, lastName);
        user.birthday = (year * 10000) + (month * 100) + day;
        user.years = 2020 - year;
        printf("%s - %d\n", user.name, user.years);
        addNodeInOrder(list, user);
    }
    fclose(usersFileCSV);
  }
};

void showList(List list) {
    if(list.first == NULL || list.last == NULL) return;

    printf("\n");
    printf("\n");
    Node *node = list.first;
    do {
        printf(" %s (%d años) -->", node->value.name, node->value.years);
        node = node->next;
    } while (node != list.first);
    printf("\n");
    printf("\n");
}

void showListInverse(List list) {
    if(list.first == NULL || list.last == NULL) return;
    printf("\n");
    printf("\n");
    Node *node = list.last;
    do{
        printf(" %s (%d años) -->", node->value.name, node->value.years);
        node = node->prev;
    }while (node != list.last);
    printf("\n");
    printf("\n");
}

void renameUser(List *list, char name[20], char newName[20]) {
    Node *currentNode = list->first;

    while (currentNode != list->last && strcmp(currentNode->value.name, name)){
        currentNode = currentNode->next;
    };
    
    if(strcmp(currentNode->value.name, name) == 0){
        strcpy(currentNode->value.name, newName);
    } else {
        printf("El valor no existe");
    }
}

void removeUser(List *list, char name[20]) {
    Node *deletedNode = list->first;

    while (deletedNode != list->last && strcmp(deletedNode->value.name, name)){
        deletedNode = deletedNode->next;
    };
    
    if(strcmp(deletedNode->value.name, name) == 0){
        if(deletedNode == list->first) {
            list->first = deletedNode->next;
            list->first->prev = list->last;
            list->last->next = list->first;
        } else if (deletedNode == list->last) {
            list->last = deletedNode->prev;
            list->last->next = list->first;
            list->first->prev = list->last;
        } else {
            deletedNode->prev->next = deletedNode->next;
            deletedNode->next->prev = deletedNode->prev;
        }
        
        free(deletedNode);
    } else {
        printf("El valor no existe");
    }
}

void deleteList(List *list) {
    int stop = 0;
    while (!stop)
    {
        if(list->first == list->first->next) stop = 1; 

        Node *deletedNode = list->first;
        list->last->next = list->first = deletedNode->next;
        list->first->prev = list->last;

        free(deletedNode);
    }

    list->first = NULL;
    list->last = NULL;

}

List *getAdultList(List *fullList) {
    List *filtredList = createList();
    if(fullList->first == NULL || fullList->last == NULL) return filtredList;

    Node *currentNode = fullList->first;

    do{
        if(currentNode->value.years >= 18) {
            addNode(filtredList, currentNode->value);
        }
        currentNode = currentNode->next;
    } while (currentNode != fullList->first);

    return filtredList;
}