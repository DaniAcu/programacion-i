#include <stdio.h>
#include <stdlib.h>
#include "list.h"

Node *createNode (int value) {
    Node *pNode = malloc(sizeof(Node));
    pNode->value = value;
    pNode->next = NULL;

    return pNode;
}

// add delete all;

void addNode(Node **head, int value) {
    Node *newNode = createNode(value);
    newNode->next = *head;
    *head = newNode;
}

void addNodeInOrder(Node **head, int value) {
    Node *newNode = createNode(value);
    Node *iNode = *head;

    while (iNode != NULL && iNode->next != NULL && iNode->next->value < value){
        iNode = iNode->next;
    }

    if(*head == NULL) {
        *head = newNode;
    }else {
        newNode->next = iNode->next;
        iNode->next = newNode; 
    }       
}

void deleteValueOnList(Node **head, int value) {
    Node *iNode = *head;
    while (iNode->next != NULL && iNode->next->value != value){       
        iNode = iNode->next;
    }

    if((*head)->value == value){
        Node *eliminado = *head;
        *head = (*head)->next;
        free(eliminado);
    } else if(iNode->next != NULL){
        Node *eliminado = iNode->next;
        iNode->next = eliminado->next;
        free(eliminado);
    } else {
        printf("El valor no existe");
    }
}

void deleteList(Node **head) {
    Node *tmp ;
    while (*head != NULL){
       tmp = *head;
       *head = (*head)->next;
       free(tmp);
    }
}

void orderListByValue(Node **head) {
    int isOrdered = 0;

    if((*head) == NULL || (*head)->next == NULL) return;

    while (isOrdered == 0){
        isOrdered = 1;
        Node *node = *head;
        while (node->next != NULL){
            if(node->value > node->next->value){
                isOrdered = 0;
                int temp = node->value;
                node->value = node->next->value;
                node->next->value = temp;
            }
            node = node->next;
        }
    } 
}

void orderListByNode(Node **head) {
    int isOrdered = 0;

    if((*head) == NULL || (*head)->next == NULL) return;

    while (isOrdered == 0){
        isOrdered = 1;
        Node *node = *head;
        Node *prevNode = NULL;
        while (node->next != NULL){
            if(node->value > node->next->value){
                Node *aux = node->next;
                node->next = aux->next;
                aux->next = node;

                if(prevNode != NULL) {
                    prevNode->next = aux;
                }
            }
            prevNode = node;
            node = node->next;
        }
    }

    printf("\n%d\n", (*head)->value);
}

void showList(Node *head) {
    printf("\n");
    Node *node = head;
    while (node != NULL){
        printf(" %d -->", node->value);
        node = node->next;
    }
}
