
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int main() {
  Node *head = NULL;
  addNode(&head, 1);
  addNode(&head, 2);
  addNode(&head, 3);

  showList(head);

  return 0;
}

