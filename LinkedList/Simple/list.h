typedef struct node {
  int value;
  struct node *next; 
} Node;

Node *createNode (int value);
void addNode(Node **head, int value);
void addNodeInOrder(Node **head, int value);
void orderListByValue(Node **head);
void orderListByNode(Node **head);
void deleteValueOnList(Node **head, int value);
void deleteList(Node **head);
void showList(Node *list);