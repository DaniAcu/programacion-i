#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "tree.h"

Queue createEmptyQueue(){
  Queue queue;
  queue.head = NULL;
  queue.tail = NULL;

  return queue;
}

Stack createEmptyStack(){
  Stack stack;
  stack.head = NULL;

  return stack;
}

void enqueue(Queue *queue, int value){
  Node *newNode = malloc(sizeof(Node));
  newNode->value = value;
  newNode->next = NULL;

  if(isEmptyQueue(*queue)) {
    queue->head = newNode;
  } else {
    queue->tail->next = newNode;
  }

  queue->tail = newNode;  
}

int dequeue(Queue *queue){
  if(isEmptyQueue(*queue)) return INT_MIN;
  Node *deleted = queue->head;
  
  if(queue->head == queue->tail) queue->tail = NULL;
  queue->head = queue->head->next;



  int value = deleted->value;

  free(deleted);

  return value;
}

int peekQueue(Queue queue){
  if(queue.head == NULL) return INT_MIN;

  return queue.head->value;
}

int isEmptyQueue(Queue queue){
  return queue.head == NULL && queue.tail == NULL;
}


void push (Stack *stack, int value){
  Node *newNode = malloc(sizeof(Node));
  newNode->value = value;
  newNode->next = stack->head;
  stack->head = newNode;
}

int pop (Stack *stack){
  if(isEmptyStack(*stack)) return INT_MIN;

  Node *deleted;
  deleted = stack->head;
  stack->head = stack->head->next;
  int value = deleted->value;

  free(deleted);

  return value;
}

int peekStack(Stack stack){
  if (isEmptyStack(stack)) return INT_MIN;

  return stack.head->value;  
}

int isEmptyStack (Stack stack){
  return stack.head == NULL;
}

TreeNode *generateTreeNode(int value){
    TreeNode *node = malloc(sizeof(TreeNode));
    node->left = NULL;
    node->right = NULL;
    node->value = value;

    return node;
}

void insertIntoTree(TreeNode **node, int value){
    if(*node == NULL) {
        *node = generateTreeNode(value);
    }

    if((*node)->value > value){
        insertIntoTree(&(*node)->left, value);
    } else if ((*node)->value < value){
        insertIntoTree(&(*node)->right, value);
    } 
}

void fillTree(TreeNode **node, Stack *stack) {
  if(isEmptyStack(*stack)) return;

  int poped = pop(stack);
  insertIntoTree(node, poped);
  fillTree(node, stack);
  push(stack, poped);
}

void printTree(TreeNode *root, int space) { 

    if (root == NULL) 
        return; 
  
    space += 10; 
  

    printTree(root->right, space); 
  

    printf("\n"); 
    for (int i = 10; i < space; i++) printf(" "); 
    printf("%d\n", root->value); 
  

    printTree(root->left, space); 
}

int getLeafCount(TreeNode *root) {
  if(root == NULL)        
    return 0; 
  if(root->left == NULL && root->right==NULL)       
    return 1;             
  else 
    return getLeafCount(root->left) + getLeafCount(root->right);
}

void getAmountOfNodesMultiple3(TreeNode *root, int *acum) {
  if(root == NULL) return;            
  
  if(root->value % 3 == 0) *acum += 1;
  getAmountOfNodesMultiple3(root->left, acum);
  getAmountOfNodesMultiple3(root->right, acum);
}

void checkBalancedTree(TreeNode *root, int *isBalanced) {
  
  if(*isBalanced == 0) return;

  if(
    root != NULL && ((root->left == NULL && root->right==NULL) || (root->left != NULL && root->right!=NULL))
  ) {
    *isBalanced = 1;
  } else {
    *isBalanced = 0;
  }


  if(root != NULL) {
    checkBalancedTree(root->left, isBalanced);
    checkBalancedTree(root->right, isBalanced);
  }   
}

int getTreeHigh(TreeNode *root) {
    if (root == NULL) return 0;

    int leftHigh = getTreeHigh(root->left);
    int rightHigh = getTreeHigh(root->right);
    return 1 + (leftHigh > rightHigh ? leftHigh : rightHigh);
}

void enqueueEnd5And7 (TreeNode *root, Queue *queue) {
  if(root == NULL) return;

  int lastDigit = root->value % 10;
  if(lastDigit == 5 || lastDigit == 7) {
    enqueue(queue, root->value);
  }
  
  enqueueEnd5And7(root->left, queue);
  enqueueEnd5And7(root->right, queue);
}

void showQueue(Queue *queue){

  Queue reversedQueue = createEmptyQueue();
  while(!isEmptyQueue(*queue)){
    int value = dequeue(queue);
    printf("%d --> ", value);
    enqueue(&reversedQueue, value);
  }

  // Go back to original order
  while(!isEmptyQueue(reversedQueue)){
    int value = dequeue(&reversedQueue);
    enqueue(queue, value);
  }
}

TreeNode *deleteLeaf(TreeNode* root) { 
    if (root == NULL) return NULL; 
    if (root->left == NULL && root->right == NULL) { 
        free(root); 
        return NULL; 
    } 
  
    root->left = deleteLeaf(root->left); 
    root->right = deleteLeaf(root->right); 
  
    return root; 
}


void showTreeInorder (TreeNode *root) {
  if (root == NULL) {
      return;
  }

  showTreeInorder(root->left);
  printf("%d ", root->value);
  showTreeInorder(root->right);
}