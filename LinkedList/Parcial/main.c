#include <stdlib.h>
#include <stdio.h>
#include "tree.h"

int main(){
  Stack stack = createEmptyStack();

  push(&stack, 10);
  push(&stack, 27);
  push(&stack, 47);
  push(&stack, 72);
  push(&stack, 83);
  push(&stack, 92);
  push(&stack, 96);
  push(&stack, 25);
  push(&stack, 39);
  push(&stack, 42);
  push(&stack, 49);
  push(&stack, 52);
  push(&stack, 77);
  push(&stack, 86);
  push(&stack, 99);
  push(&stack, 38);
  push(&stack, 44);
  push(&stack, 69);
  push(&stack, 90);
  push(&stack, 40);
  push(&stack, 85);
  push(&stack, 50);


  TreeNode *root = NULL;
  fillTree(&root, &stack);

  printf("Hojas del arbol: %d\n", getLeafCount(root));

  int multiple3Nodes = 0;
  getAmountOfNodesMultiple3(root, &multiple3Nodes);

  printf("Nodos multiplos de 3: %d\n", multiple3Nodes);

  int isBalancedTree;

  checkBalancedTree(root, &isBalancedTree);

  printf("Arbol balanceado: %s\n", isBalancedTree ? "SI" : "NO");

  printf("Altura del arbol: %d\n", getTreeHigh(root));

  Queue queue = createEmptyQueue();

  enqueueEnd5And7(root, &queue);

  printf("Cola: "); showQueue(&queue); printf("\n");

  deleteLeaf(root->right);

  printf("Tree: "); showTreeInorder(root); printf("\n\n\n");
  
  char yes;

  printf("Desea ver el arbol graficado? (S/n): ");
  scanf("%c", &yes);

  if(yes == 's' || yes == 'S') {
    printTree(root, 0);
  }


  return 0;
}
