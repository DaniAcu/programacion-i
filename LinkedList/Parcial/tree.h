typedef struct node {
  int value;
  struct node *next;
} Node;

typedef struct {
  struct node *head;
  struct node *tail;
}Queue;

typedef struct {
  struct node *head;
}Stack;

typedef struct treenode {
    struct treenode *right;
    struct treenode *left;
    int value;
} TreeNode;

Queue createEmptyQueue();
Stack createEmptyStack();
void enqueue(Queue *queue, int value);
int dequeue(Queue *queue);
int peekQueue(Queue queue);
int isEmptyQueue(Queue queue);
void push (Stack *stack, int value);
int pop (Stack *stack);
int peekStack(Stack stack);
int isEmptyStack(Stack stack);
TreeNode *generateTreeNode(int value);
void insertIntoTree(TreeNode **node, int value);
void fillTree(TreeNode **node, Stack *stack);
void printTree(TreeNode *root, int space);
int getLeafCount(TreeNode *root);
void getAmountOfNodesMultiple3(TreeNode *root, int *acum);
void checkBalancedTree(TreeNode *root, int *isBlanced);
int getTreeHigh(TreeNode *root);
void enqueueEnd5And7 (TreeNode *root, Queue *queue);
void showQueue(Queue *queue);
TreeNode *deleteLeaf(TreeNode* root);
void showTreeInorder (TreeNode *root);

