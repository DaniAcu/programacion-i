#define MAX_DESCRIPTION 50

typedef struct {
  int id;
  int area;
  char description[MAX_DESCRIPTION];
  float costPrice;
  float sellPrice;
  int stock;
} Article;

typedef struct node {
  Article value;
  struct node *next;
} Node;

Node *createNode (Article article);
void addNodeInOrder(Node **head, Article value);
void generateMissingList(Node *list, Node **missingList);
void deleteValueOnList(Node **head, int value);
void deleteList(Node **head);
void showList(Node *list);
void showListAndArea(Node *head);

void loadArticles(Node **head);
void showArticle (Article user);