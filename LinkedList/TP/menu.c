#include <stdio.h>
#include <stdlib.h>
#include "menu.h"

int getOption() {
  int opcion;
  do {
    system("@cls||clear");
    printf("\n\n=========== Registros - Menu ===========\n \n");
    printf("Elija un tarea:\n");
    printf("\t 1: Mostrar la lista cargada e indicar cuantos artículos hay en cada rubro\n");
    printf("\t 2: Generar una nueva lista que se llame 'faltantes' que contenga solo los código de artículo del rubro 'electrónica' cuyo stock es inferior a 100.\n");
    printf("\t 3: Mostrar la lista 'faltantes'.\n");
    printf("\t 4: Eliminar de la lista un artículo determinado indicado por el usuario\n");
    printf("0: SALIR\n");
    scanf("%d", &opcion);
  } while (opcion < 0);
  return opcion;
}

void pressAnyKey () {
  printf("\nPresione Enter para volver al menu ...");
  while(getchar()!='\n');
  getchar();
}