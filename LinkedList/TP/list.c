#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "file.h"
#define FILENAME "ARTICULOS.txt"
#define MAX_DESCRIPTION 50

Node *createNode (Article article){
    Node *pNode = malloc(sizeof(Node));
    pNode->value = article;
    pNode->next = NULL;

    return pNode;
}

void addNodeInOrder(Node **head, Article article) {
    Node *newNode = createNode(article);
    Node *iNode = *head;

    while (iNode != NULL && iNode->next != NULL && iNode->next->value.id < article.id){
        iNode = iNode->next;
    }

    if(*head == NULL) {
        *head = newNode;
    }else {
        newNode->next = iNode->next;
        iNode->next = newNode; 
    }       
}

void deleteValueOnList(Node **head, int id) {
    Node *iNode = *head;
    while (iNode->next != NULL && iNode->next->value.id != id){       
        iNode = iNode->next;
    }

    if((*head)->value.id == id){
        Node *eliminado = *head;
        *head = (*head)->next;
        free(eliminado);
    } else if(iNode->next != NULL){
        Node *eliminado = iNode->next;
        iNode->next = eliminado->next;
        free(eliminado);
    } else {
        printf("El valor no existe");
    }
}

void deleteList(Node **head) {
    Node *tmp ;
    while (*head != NULL){
       tmp = *head;
       *head = (*head)->next;
       free(tmp);
    }
}

void generateMissingList(Node *list, Node **missingList) {
    Node *node = list;
    while (node != NULL){
        if(node->value.area == 1 && node->value.stock < 100) {
            addNodeInOrder(missingList, node->value);
        }
        node = node->next;
    }
}

void showList(Node *head) {
    printf("\n");
    Node *node = head;
    while (node != NULL){
        showArticle(node->value);
        printf("\n-->\n");
        node = node->next;
    }
}

void showListAndArea(Node *head) {
    int amount1 = 0, amount2 = 0, amount3 = 0, amount4 = 0;
    printf("\n");
    Node *node = head;
    while (node != NULL){
        showArticle(node->value);
        printf("\n-->\n");

        //printf("", node->value.id, node->value.area);
        switch (node->value.area){
            case 1:
                amount1++;
            break;
            case 2:
                amount2++;
            break;
            case 3:
                amount3++;
            break;
            case 4:
                amount4++;
            break;
        }
        node = node->next;
    }

    printf("\nArticulos por rubro:\n\n");
    printf("electrónica: %d\n", amount1);
    printf("computación: %d\n", amount2);
    printf("audio: %d\n", amount3);
    printf("telefonía celular: %d\n", amount4);    
}

void loadArticles(Node **head) {
  FILE *articlesFile = openFile(FILENAME, "r");
    
  if(articlesFile != NULL){
    Article article;
    int id, area, stock;
    float costPrice;
    char description[MAX_DESCRIPTION] = "";

    /*
    123,1,Description here,17.5,20.5,5\n
    
    */

    while(feof(articlesFile) == 0){
        fscanf(articlesFile, "%d,%d,%[^,],%f,%d\n", &id, &area, description, &costPrice, &stock);
        article.id = id;
        article.area = area;
        strcpy(article.description, description);
        article.costPrice = costPrice;
        article.sellPrice = costPrice + (costPrice / 4);
        article.stock = stock;
        addNodeInOrder(head, article);
    }
    fclose(articlesFile);
  }
}



void showArticle (Article article) {
  printf("ID: %d\n", article.id);
  printf("Area: %d\n", article.area);
  printf("Descrption: %s\n", article.description);    
  printf("Cost Price: %.2f\n", article.costPrice);
  printf("Sell Price: %.2f\n", article.sellPrice);
  printf("Stock: %d\n", article.stock);
}