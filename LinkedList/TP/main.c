
#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "menu.h"

int main() {
  Node *mainList = NULL;
  Node *listMissing = NULL;
  loadArticles(&mainList);

  int opcion, id;
  
  while ((opcion = getOption())) {
    switch (opcion) {
      case 1:
        showListAndArea(mainList);
      break;
      case 2:
        generateMissingList(mainList, &listMissing);
      break;
      case 3:
        showList(listMissing);
      break;
      case 4:
          printf("Ingresar codigo del articulo a eliminar: ");
          scanf("%d", &id);
          deleteValueOnList(&mainList, id);
      break;
      case 0:
        printf("El programa ha finalizado.\n");
        deleteList(&listMissing);
        deleteList(&mainList);
      break;
    }

    pressAnyKey();
  }

  return 0;
}

