#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

void cleanBuffer() {
  fflush(stdin);
  while(getchar()!='\n');
}

void readLine (char *str) {
    cleanBuffer();
    scanf("%[^\n]", str);
}

FILE *openFile(const char *filename, const char *mode) {
    FILE *file = fopen(filename, mode);
    if (file == NULL){
        printf("Can't open %s. Please check the permissions or create the file", filename);
    }

    return file;
}

void saveFile(FILE *file) {
    int erros = fclose(file);
    if (erros == 0){
        printf("Archivo guardado correctamente!\n");
    }
}

int getOption() {
  int opcion;
  do
  {
    system("@cls||clear");
    printf("\n\n=========== Equipos - Grupo 3 ===========\n \n");
    printf("Elija un tarea:\n");
    printf("\t 1: Cargar equipos\n");
    printf("\t 2: Listar por pantalla los equipos que superan el promedio de puntos.\n");
    printf("\t 3: Listar por pantalla los equipos invictos.\n");
    printf("0: SALIR\n");
    scanf("%d", &opcion);
  } while (opcion < 0 || opcion > 3);
  return opcion;
}

void pressAnyKey () {
  printf("\nPresione Enter para volver al menu ...");
  cleanBuffer();
  getchar();
}