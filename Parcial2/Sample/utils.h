#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED
void cleanBuffer();
void readLine (char *str);
FILE *openFile(const char *filename, const char *mode);
void saveFile(FILE *file);
int getOption();
void pressAnyKey();
#endif // UTILS_H_INCLUDED