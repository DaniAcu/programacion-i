#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "team.h"
#define FILENAME "teams.dat"
#define FILTERFILE "temp_users.dat"

void loadTeam () {
  FILE *teamsFile = openFile(FILENAME, "ab+");
  int moreValues = 0;
  if(teamsFile != NULL) {
    printf("\nINGRESE DATOS DEL USUARIO\n");
    do
    {
      Team team = getTeam();

      if(!fwrite(&team, sizeof(Team), 1, teamsFile)){
        printf("Error: No pudimos guardar su usuario, intente nuevamente.");
      }
      printf("\n\nIngrese 1 si desea ingresar más valores: ");
      scanf("%d", &moreValues);
    } while (moreValues == 1);

    saveFile(teamsFile);
  }
}

Team getTeam () {
    Team team;
    printf("Nombre: ");
    readLine(team.name);
    int matchAmount = 0;
    do
    {
      printf("Ingresar cantidad de partidos jugados (12 maximos): ");
      cleanBuffer();
      scanf("%d", &matchAmount);
      if(matchAmount <= 0 || matchAmount > 12) {
        printf("Valor ingresado invalido\n");
      }
    } while (matchAmount <= 0 || matchAmount > 12);
    
    team.matches = matchAmount;

    for (int i = 0; i < team.matches; i++){
      char result;
      do {
        printf("Ingresar resultado del partidos %d (‘G’ = ganado, ‘E’ = empate, ‘P’ = perdido): ", i);
        cleanBuffer();
        scanf("%c", &result);
        if(result != 'G' && result != 'E' && result != 'P'){
          printf("Valor ingresado invalido\n");
        }
      }while(result != 'G' && result != 'E' && result != 'P');
      
      team.matchesResults[i] = result;
    }

    return team;
}

float getAverageScore() {
  FILE *teamsFile = openFile(FILENAME, "rb");
  int totalTeams = 0;
  float totalScore = 0, totalTeamAverageScore = 0;
  if(teamsFile != NULL){
    Team team;
    while(fread(&team, sizeof(Team), 1, teamsFile)){
        totalTeams++;
        float teamAverageScore = getTeamAverageScore(team);
        totalScore += teamAverageScore;
        teamAverageScore = 0;
    }

    totalTeamAverageScore = totalScore / totalTeams;

    fclose(teamsFile);

    return totalTeamAverageScore;
  }

  return 0;
}

void listTeamHighAverage () {
  float averageScore = getAverageScore();
  FILE *teamsFile = openFile(FILENAME, "rb");
  if(teamsFile != NULL || averageScore == 0){
      Team team;
      
      printf("\nEquipos que superan el promedio\n");
  
      while(fread(&team, sizeof(Team), 1, teamsFile)){
        float teamScore = getTeamAverageScore(team);

        if(teamScore > averageScore) {
          printf("- %s\n", team.name);
        }
      }

      fclose(teamsFile);
  } else {
    printf("\nNo se pudo obtener el average\n");
  }
}

void listTeamWinners() {
  FILE *teamsFile = openFile(FILENAME, "rb");
  if(teamsFile != NULL){
      Team team;
      int matchesWon = 0;
      int indexMatch = 0;
      printf("\nEquipos invictos\n");
      while(fread(&team, sizeof(Team), 1, teamsFile)){
        do {
          if(team.matchesResults[indexMatch] == 'G'){
            matchesWon++;
          }
          indexMatch++;
        }while(
          team.matchesResults[indexMatch] != 'G' 
          && team.matchesResults[indexMatch] != 'E'
          && team.matchesResults[indexMatch] != 'P'
        );

        if(matchesWon == indexMatch) {
          printf("- %s\n", team.name);
        }
      }

      fclose(teamsFile);
  }
}

float getTeamAverageScore(Team team) {
  int indexMatch = 0;
  int teamScore = 0;
  do {
    switch (team.matchesResults[indexMatch]){
      case 'G':
        teamScore += 3;
        break;
      case 'E':
        teamScore += 1;
        break;
      case 'P':
        teamScore += 0;
        break;
    }
    indexMatch++;
  }while(
    team.matchesResults[indexMatch] == 'G' 
    || team.matchesResults[indexMatch] == 'E'
    || team.matchesResults[indexMatch] == 'P'
  );

  return (teamScore*1.0) / indexMatch;
}