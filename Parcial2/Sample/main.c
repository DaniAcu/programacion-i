#include <stdio.h>
#include <stdlib.h>
#include "utils.h"
#include "team.h"

int main() {
  int opcion;
  
  while ((opcion = getOption())) {
    switch (opcion) {
      case 1:
        loadTeam();
        break;
      case 2:
        listTeamHighAverage();
        break;
      case 3:
        listTeamWinners();
        break;
      case 0:
        printf("El programa ha finalizado.\n");
        break;
    }

    pressAnyKey();
  }
  return 0;
}
