typedef struct {
  char name[30];
  int score;
} TeamResult;

typedef struct {
  char name[30];
  int matches;
  char matchesResults[12];
} Team;

void loadTeam();
void listTeamHighAverage();
void listTeamWinners();
Team getTeam();
float getAverageScore();
float getTeamAverageScore(Team team);