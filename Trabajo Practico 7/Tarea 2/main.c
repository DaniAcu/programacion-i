#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#define MAX_LENGTH 5

typedef struct {
  int brandId;
  char model[20];
  int year;
  int fuelType;
  float price;
} Car;

void pressAnyKey ();
int getOption();
void setBrand(int *id);
char * getFuelType(int type);
void setFuelType(int *type);
float getPrice(float origialPrice, int year);
void loadCars(Car carList[MAX_LENGTH]);
void showCar(Car car);
void showCarList(Car carList[MAX_LENGTH]);
void showCarByModel(Car carList[MAX_LENGTH]);
void reducePrice(Car carList[MAX_LENGTH]);
void loadMatrix(Car carMatrix[5][MAX_LENGTH], Car carList[MAX_LENGTH]);
void showMatrix(Car carMatrix[5][MAX_LENGTH]);

char brands[5][15] = {"Fiat", "Renault", "Ford", "Toyota", "Citroen"};

int main() {
  setlocale(LC_ALL,""); // Enabled UTF-8 (You could use ñ and other special characters)
  
  Car carList[MAX_LENGTH];
  Car carMatrix[5][MAX_LENGTH];
  int opcion;
  
  while (opcion = getOption()) {
    switch (opcion) {
      case 1:
        loadCars(carList);
        break;
      case 2:
        showCarByModel(carList);
        break;
      case 3:
        reducePrice(carList);
        break;
      case 4:
        loadMatrix(carMatrix, carList);
        showMatrix(carMatrix);
        break;
      case 0:
        printf("El programa ha finalizado.\n");
        break;
    }

    pressAnyKey();
  }

  return 0;
}

void pressAnyKey () {
  printf("\nPresione una tecla para volver al menu ...");
  while(getchar()!='\n');
  getchar();
}

int getOption() {
  int opcion;
  do
  {
    system("@cls||clear");
    printf("\n\n=========== Registros - Menu ===========\n \n");
    printf("Elija un tarea:\n");
    printf("\t 1: Cargar autos\n");
    printf("\t 2: Mostrar precio de autos por modelo\n");
    printf("\t 3: Reducir precios de autos por tipo de combustible\n");
    printf("\t 4: Crear y mostrar matriz.\n");
    printf("0: SALIR\n");
    scanf("%d", &opcion);
  } while (opcion < 0);
  return opcion;
}

void setBrand(int *id) {
  int tmp = 0;
  printf("Ingrese la marca del auto\n");
  for (int i = 1; i <= 5; i++){
    printf("\t%d. %s\n", i, brands[i - 1]);
  }
  printf("Elija una opcion: ");
  scanf("%d", &tmp); 
  while(tmp < 1 || tmp > 5) {
    printf("\nOpcion incorrecta. Intente nuevamente.\n");
    printf("Elija una opcion: ");
    scanf("%d", &tmp); 
  }

  *id = tmp;
}

char * getFuelType(int type) {
  switch (type) {
  case 1:
    return "GNC";
  case 2:
    return "Nafta";
  case 3:
    return "Diesel";
  }
}

void setFuelType(int *type) {
  int tmp = 0;
  printf("Ingrese la tipo del combustible\n");
  for (int i = 1; i <= 3; i++){
    printf("\t%d. %s\n", i, getFuelType(i));
  }
  printf("Elija una opcion: ");
  scanf("%d", &tmp); 
  while(tmp < 1 || tmp > 3) {
    printf("\n Opcion incorrecta. Intente nuevamente.\n");
    printf("Elija una opcion: ");
    scanf("%d", &tmp); 
  }

  *type = tmp;
}

float getPrice(float origialPrice, int year) {
  if(year < 2000 && year > 1997){
    return origialPrice + ((origialPrice / 100) * 15);
  } else if (year <= 2000) {
    return origialPrice + ((origialPrice / 100) * 18);
  } else {
    return origialPrice + ((origialPrice / 100) * 10);
  }
}

void loadCars(Car carList[MAX_LENGTH]) {
  printf("\n======== Cargar Autos ========\n");
  for (int i = 0; i < MAX_LENGTH; i++) {
    printf("CAR %d \n\n", i);
    setBrand(&carList[i].brandId);
    printf("Ingrese modelo del auto:");
    scanf(" %[^\n]", &carList[i].model);
    printf("Ingrese año del auto:");
    scanf("%d", &carList[i].year);
    setFuelType(&carList[i].fuelType);
    printf("Ingrese precio del auto:");
    scanf("%f", &carList[i].price);
    system("@cls || clear");
    printf("\n=============================\n");
    fflush(stdin);
  }
}

void showCar(Car car) {
  printf("modelo: %s\n", car.model);
  printf("año: %d\n", car.year);
  printf("tipo de combustible: %s\n", getFuelType(car.fuelType));
  printf("precio de venta: %.2f\n", getPrice(car.price, car.year));
}

void showCarByModel(Car carList[MAX_LENGTH]) {
  printf("\n======== Mostrar Precio de los Autos por Modelo ========\n");
  int brandId;
  setBrand(&brandId);
  printf("Marca: %s\n", brands[brandId - 1]);
  printf("===========================\n");

  for (int i = 0; i < MAX_LENGTH; i++) {
    if(carList[i].brandId == brandId) {
      printf("Auto %d \n\n", i);
      showCar(carList[i]);
      printf("===========================\n");
    }
  }
}

void reducePrice(Car carList[MAX_LENGTH]) {
  printf("\n======== Reducir Precio de los Autos por Tipo de Combustible ========\n");
  int fuelType;
  setFuelType(&fuelType);
  printf("Tipo de combustible: %s\n", getFuelType(fuelType));
  float percentage = 0;
  printf("Ingrese porcentaje a reducir: ");
  scanf("%f", &percentage);

  for (int i = 0; i < MAX_LENGTH; i++) {
    if(carList[i].fuelType == fuelType) {
      carList[i].price = carList[i].price - ((carList[i].price / 100) * percentage);
    }
  }

  printf("\nActualizado con exito!\n");
}

void loadMatrix(Car carMatrix[5][MAX_LENGTH], Car carList[MAX_LENGTH]) {
  int carIndex = 0;
  Car dump;
  dump.brandId = 0;
  for (int i = 0; i < 4; i++){
    for (int j = 0; j < MAX_LENGTH; j++){
      if(carList[j].brandId == i + 1){
        carMatrix[i][carIndex] = carList[j];
        carIndex++;
      }else {
        carMatrix[i][carIndex] = dump;
      }
    }
    carIndex = 0;
  }  
}

void showMatrix(Car carMatrix[5][MAX_LENGTH]) {
  int carIndex = 0;
  for (int i = 0; i < 4; i++){
    printf("========================\n");
    printf("Marca %s\n", brands[i]);
    while(carMatrix[i][carIndex].brandId != 0) {
        printf("--------------------------------\n");
        printf("Auto %dx%d\n", i, carIndex);
        showCar(carMatrix[i][carIndex]);
        carIndex++;
    }
    carIndex = 0;
  }  
}

