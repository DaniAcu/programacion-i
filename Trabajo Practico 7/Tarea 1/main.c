#include <stdio.h>
#include <stdlib.h>
#define MAX_LENGHT 50

typedef struct {
  int id;
  float cost;
  char description[MAX_LENGHT];
} Article;

typedef struct {
  int id;
  float price;
} SaleArticle;

void pressAnyKey ();
int getOption();
float get25Porcent (float number);
void createSaleArticleList(SaleArticle saleArticleList[MAX_LENGHT], Article articleList[MAX_LENGHT], int articleListLenght);
int getArticles(Article articleList[MAX_LENGHT]);
void showFilterList(SaleArticle saleArticleList[MAX_LENGHT], Article articleList[MAX_LENGHT], int articleListLenght);

int main() {
  int opcion, articleListLenght;
  Article articleList[MAX_LENGHT];
  SaleArticle saleArticleList[MAX_LENGHT];

  while (opcion = getOption())
  {
    switch (opcion)
    {
    case 1:
      articleListLenght = getArticles(articleList);
      break;
    case 2:
      createSaleArticleList(saleArticleList, articleList, articleListLenght);
      break;
    case 3:
      showFilterList(saleArticleList, articleList, articleListLenght);
      break;
    case 0:
      printf("El programa ha finalizado.\n");
      break;
    }

    pressAnyKey();
  }
  
  return 0;
}

void pressAnyKey () {
  printf("\nPresione una tecla para volver al menu ...");
  while(getchar()!='\n');
  getchar();
}

int getOption()
{
  int opcion;
  do
  {
    system("@cls||clear");
    printf("\n\n=========== Registros - Menu ===========\n \n");
    printf("Elija un tarea:\n");
    printf("\t 1: Cargar articulos\n");
    printf("\t 2: Generar lista con articulos a la venta\n");
    printf("\t 3: Mostar los articulos con precios mayores a $1500\n");
    printf("0: SALIR\n");
    scanf("%d", &opcion);
  } while (opcion < 0);
  return opcion;
}

float get25Porcent (float number) {
  return (number / 100) * 25;
}

void createSaleArticleList(SaleArticle saleArticleList[MAX_LENGHT], Article articleList[MAX_LENGHT], int articleListLenght) {
  for (int i = 0; i < articleListLenght; i++){
    saleArticleList[i].id = articleList[i].id;
    saleArticleList[i].price = articleList[i].cost + get25Porcent(articleList[i].cost);
  }
  printf("\nGenerado con exitos.\n");
}

int getArticles (Article articleList[MAX_LENGHT]) {
  int index = 0;
  char flag;

  do {
    printf("¿Desea ingresar valores? (Y/N): ");
    scanf(" %c", &flag);

    if(flag == 'Y'){
      Article my_article;
      printf("Ingrese numero del articulo: ");
      scanf("%d", &my_article.id);

      printf("Ingrese costo de fabricacion del articulo: $");
      scanf("%f", &my_article.cost);

      printf("Ingrese descripcion del articulo: ");
      scanf(" %[^\n]", my_article.description);

      articleList[index] = my_article;

      index++;
    }

    fflush(stdin);
  } while (flag != 'N' && index <= MAX_LENGHT);

  if(index == MAX_LENGHT) printf("Usted ha llegado al limite de articulos permitidos");

  return index;
}

void showFilterList(SaleArticle saleArticleList[MAX_LENGHT], Article articleList[MAX_LENGHT], int articleListLenght) {
  printf("Estos articulos supera los $1500 de precio de venta.");
  for (int i = 0; i < articleListLenght; i++){
    if(saleArticleList[i].price > 1500) {
      printf("\n====== Articulo ======\n");
      printf("cost: $%.2f\n", saleArticleList[i].price);
      printf("description: %s\n", articleList[i].description);
    }
  }
}
