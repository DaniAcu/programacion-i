#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main()
{
  setlocale(LC_ALL, "UTF-8");
  FILE *file, *newFile;
  char c;
  file = fopen("final.txt", "r");
  newFile = fopen("newFile.txt", "w");
    if (file != NULL && newFile != NULL){
      fscanf(file, "%c", &c);
      do {
        if(c == 'N') {
          fprintf(newFile, "%s", "GN");
        } else if (c == 'n') {
          fprintf(newFile, "%s", "gn");
        } else {
          fprintf(newFile, "%c", c);
        }
        fscanf(file, "%c", &c);
      } while(!feof(file));

      fclose(file);
      fclose(newFile);
      remove("final.txt");
      rename("newFile.txt", "final.txt");
    }
  return 0;
}