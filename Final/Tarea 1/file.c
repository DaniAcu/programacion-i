#include <stdio.h>
#include <stdlib.h>

FILE *openFile(const char *filename, const char *mode) {
    FILE *file = fopen(filename, mode);
    if (file == NULL){
        printf("Can't open %s. Please check the permissions or create the file", filename);
    }

    return file;
}

void saveFile(FILE *file) {
    int erros = fclose(file);
    if (erros == 0){
        printf("Archivo guardado correctamente!\n");
    }
}