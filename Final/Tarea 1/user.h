typedef struct {
  int id;
  char name[20];
  int birthday;
  int years;
} User;

User getUser (int *id);
void showUser (User user);