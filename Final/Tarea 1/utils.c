#include <stdio.h>
#include "utils.h"

void cleanBuffer() {
  while(getchar()!='\n');
}

void readLine (char *str) {
    cleanBuffer();
    scanf("%[^\n]", str);
}