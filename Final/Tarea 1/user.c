#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "user.h"
#include "file.h"
#define FILENAME_CSV "users.csv"
#define FILENAME "users.dat"
#define FILTERFILE "temp_users.dat"

User getUser (int *id) {
    User user;
    printf("Nombre: ");
    readLine(user.name);
    cleanBuffer();
    printf("Fecha de nacimiento (YYYYMMDD): ");
    scanf("%d", &user.birthday);

    int birthdayYear = user.birthday / 10000;

    user.years = 2020 - birthdayYear;
    if(id != NULL) user.id = (*id) + 1;

    return user;
}

void showUser (User user) {
  printf("ID: %d\n", user.id);
  printf("Nombre: %s\n", user.name);
  int year = user.birthday / 10000,
      month = (user.birthday % 10000) / 100,
      day = (user.birthday % 10000) % 100;
    
  printf("Fecha de Nacimiento: %02d/%02d/%4d\n", day, month, year);
  printf("Edad: %d\n", user.years);
}
