typedef struct node {
  int value;
  struct node *right;
  struct node *left; 
} Node;

typedef struct queuenode {
  Node *value;
  struct queuenode *next;
} QueueNode;

typedef struct {
  struct queuenode *head;
  struct queuenode *tail;
  int size;
}Queue;

Node *createNode (int value);
void addNode(Node **head, int value);
void countTree(Node *root, int *counter);
void showGrade2(Node *root, int initialGrade);
int getSizeTree(Node *root);
void show(Node *root);
void iterateTree (Queue *queue, int size);
void printVerticalTree(Node *root);
void printTree(Node *root, int space);
void enqueue(Queue *queue, Node *value);
Node *dequeue(Queue *queue);