#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

Node *createNode (int value) {
    Node *node = malloc(sizeof(Node));
    node->value = value;
    node->right = NULL;
    node->left = NULL;

    return node;
}

// add delete all;

void addNode(Node **head, int value) {
    if(*head == NULL) {
        *head = createNode(value);
    } else if((*head)->value < value) {
        addNode(&(*head)->right, value);
    } else {
       addNode(&(*head)->left, value);
    }
}

void show(Node *root) {
    if(root == NULL) return;

    show(root->left);
    printf("%d ", root->value);
    show(root->right);
}

void showGrade2(Node *root, int grade) {
    if(root == NULL) return;

    grade += 1;
    if(grade == 2) {
        printf("%d ", root->value);
    }
    showGrade2(root->left, grade);
    showGrade2(root->right, grade);
}

void countTree(Node *root, int *counter) {
    if(root == NULL) return;

    *counter += 1;
    countTree(root->left, counter);
    countTree(root->right, counter);
}

int getSizeTree(Node *root){
    int size = 0;
    countTree(root, &size);

    return size;
}

void iterateTree (Queue *queue, int size) {
    Queue newLevelQueue;
    newLevelQueue.head = NULL;
    newLevelQueue.tail = NULL;
    newLevelQueue.size = 0;
    while (queue->head != NULL && queue->tail != NULL) {
        Node *node = dequeue(queue);
        for (int i = 5; i < ((size / (queue->size + 1)) * 5); i++) printf(" ");
        printf("%d ", node->value);
        if(node->right != NULL) enqueue(&newLevelQueue, node->right);
        if(node->left != NULL) enqueue(&newLevelQueue, node->left);
    }
    printf("\n");
    queue = &newLevelQueue;
    if(queue->head != NULL && queue->tail != NULL) iterateTree(queue, size);
}

void printVerticalTree(Node *root) {
    if(root == NULL) return;

    int size = getSizeTree(root);

    Queue queue;
    queue.head = NULL;
    queue.tail = NULL;
    queue.size = 0;

    printf("vERTCAL?\n");

    for (int i = 5; i < (size * 5); i++) printf(" ");
    
    printf("%d\n", root->value);

    enqueue(&queue, root->right);
    enqueue(&queue, root->left);

    iterateTree(&queue, size);
}

void printTree(Node *root, int space) { 
    // Base case 
    if (root == NULL) 
        return; 
  
    // Increase distance between levels 
    space += 10; 
  
    // Process left child first 
    printTree(root->left, space); 
  
    // Print current node after space 
    // count 
    printf("\n"); 
    for (int i = 10; i < space; i++) printf(" "); 
    printf("%d\n", root->value); 
  
    // Process right child 
    printTree(root->right, space); 
    
} 


// QUEUE

void enqueue(Queue *queue, Node *value){
  QueueNode *newNode = malloc(sizeof(QueueNode));
  newNode->value = value;
  newNode->next = NULL;

  if(queue->head == NULL && queue->tail == NULL) {
    queue->head = newNode;
  } else {
    queue->tail->next = newNode;
  }

  queue->tail = newNode;  
  queue->size +=1; 
}

Node *dequeue(Queue *queue){
  if(queue->head == NULL && queue->tail == NULL) return NULL;
  QueueNode *deleted = queue->head;
  
  if(queue->head == queue->tail) queue->tail = NULL;
  queue->head = queue->head->next;

  Node *value = deleted->value;

  free(deleted);

  queue->size -=1; 

  return value;
}