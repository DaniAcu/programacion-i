
#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

int main() {
  Node *head = NULL;
  addNode(&head, 5);
  addNode(&head, 10);
  addNode(&head, 2);
  addNode(&head, 1);
  addNode(&head, 3);
  addNode(&head, 6);
  show(head);

  printf("\n");

  printTree(head, 0);

  printVerticalTree(head);

  return 0;
}

