#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "user.h"
#include "file.h"
#define FILENAME_CSV "users.csv"
#define FILENAME "users.dat"
#define FILTERFILE "temp_users.dat"

void deleteUser () {
  User user;
  char name[20];
  FILE *usersFile = openFile(FILENAME, "rb+");
  FILE *tempUsersFile = openFile(FILTERFILE, "wb+");
  if(usersFile && tempUsersFile) {
    printf("Eliminar datos de usuario por nombre.\n");

    printf("\nBuscar por nombre: ");
    readLine(name);

    while(fread(&user, sizeof(User), 1, usersFile)){
        if(strcmp(name, user.name) != 0){
          fwrite(&user, sizeof(User), 1, tempUsersFile);
        }
    }

    fclose(usersFile);
    saveFile(tempUsersFile);
    remove(FILENAME);
    rename(FILTERFILE, FILENAME);
  }  
}

void updateUser () {
  User tmp, userToUpdate;
  int pos = -1, flag = 0;
  char name[20];
  FILE *usersFile = openFile(FILENAME, "rb+");
  if(usersFile){
    printf("Actualizar datos de usuario por nombre.\n");

    printf("\nBuscar por nombre: ");
    readLine(name);

    while(flag == 0 && fread(&tmp, sizeof(User), 1, usersFile)){
        pos++;
        if(strcmp(name, tmp.name) == 0) {
            flag = 1;
            userToUpdate = tmp;
        }
    }

    if(flag == 0){
        printf("Usuario no encontrado");
    }else {
        fseek(usersFile,pos * sizeof(User), SEEK_SET);

        showUser(userToUpdate);

        printf("\nActualize DATOS DEL USUARIO\n");
        User newUser = getUser(NULL);
        newUser.id = userToUpdate.id;        

        if(fwrite(&newUser, sizeof(User), 1, usersFile)) {
            saveFile(usersFile);
            printf("Succes: Pudimos guardar su usuario correctamente!");
        }else {
            printf("Error: No pudimos guardar su usuario, intente nuevamente.");
        }
    }
  }
}

void listUsers() {
  FILE *usersFile = openFile(FILENAME, "rb");
  if(usersFile != NULL){
    User user;

    while(fread(&user, sizeof(User), 1, usersFile)){
        showUser(user);
    }

    fclose(usersFile);
  }
}

void createCSV() {
  FILE *usersFile = openFile(FILENAME, "rb");
  FILE *usersFileCSV = openFile(FILENAME_CSV, "w");
    
  if(usersFile != NULL && usersFileCSV != NULL){
    User user;

    fputs("ID,NOMBRE,FECHA DE NACIMIENTO,EDAD\n", usersFileCSV);

    while(fread(&user, sizeof(User), 1, usersFile)){
        int year = user.birthday / 10000,
            month = (user.birthday % 10000) / 100,
            day = (user.birthday % 10000) % 100;
        fprintf(usersFileCSV, "%d,%s,%d/%d/%d,%d\n", user.id, user.name, day, month, year, user.years);
    }
    fclose(usersFile);
    saveFile(usersFileCSV);
  }
}

void loadCSV() {
  FILE *usersFile = openFile(FILENAME, "wb");
  FILE *usersFileCSV = openFile(FILENAME_CSV, "r");
    
  if(usersFile != NULL && usersFileCSV != NULL){
    User user;
    int id, day, month, year, userYears;
    char name[50] = "";

    fscanf(usersFileCSV, "%[^\n]", name);

    /*
    ID,NOMBRE,FECHA DE NACIMIENTO,EDAD\n
    1,Daniel Acuña, 31/08/1998, 22\n
    
    */

    while(feof(usersFileCSV) == 0){
        fscanf(usersFileCSV, "%d,%[^,],%d/%d/%d,%d\n", &id, name, &day, &month, &year, &userYears);
        user.id = id;
        strcpy(user.name, name);
        user.birthday = (year * 10000) + (month * 100) + day;
        user.years = userYears;
        showUser(user);
        fwrite(&user, sizeof(User), 1, usersFile);
    }
    saveFile(usersFile);
    fclose(usersFileCSV);
  }
}

void createUser(int *id) {
  FILE *usersFile = openFile(FILENAME, "ab+");
  if(usersFile != NULL) {
    printf("\nINGRESE DATOS DEL USUARIO\n");
    User user = getUser(id);

    if(fwrite(&user, sizeof(User), 1, usersFile)){
        saveFile(usersFile);
        (*id) = (*id) + 1;
    } else {
        printf("Error: No pudimos guardar su usuario, intente nuevamente.");
    }
  }
}

User getUser (int *id) {
    User user;
    printf("Nombre: ");
    readLine(user.name);
    cleanBuffer();
    printf("Fecha de nacimiento (YYYYMMDD): ");
    scanf("%d", &user.birthday);

    int birthdayYear = user.birthday / 10000;

    user.years = 2020 - birthdayYear;
    if(id != NULL) user.id = (*id) + 1;

    return user;
}

void showUser (User user) {
  printf("ID: %d\n", user.id);
  printf("Nombre: %s\n", user.name);
  int year = user.birthday / 10000,
      month = (user.birthday % 10000) / 100,
      day = (user.birthday % 10000) % 100;
    
  printf("Fecha de Nacimiento: %02d/%02d/%4d\n", day, month, year);
  printf("Edad: %d\n", user.years);
}
