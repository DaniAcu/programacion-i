#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "utils.h"

int getOption() {
  int opcion;
  do
  {
    system("@cls||clear");
    printf("\n\n=========== Registros - Menu ===========\n \n");
    printf("Elija un tarea:\n");
    printf("\t 1: Monstrar usuarios\n");
    printf("\t 2: Registrar un usuario\n");
    printf("\t 3: Actualizar datos de un usuario\n");
    printf("\t 4: Eliminar un usuario\n");
    printf("\t 5: Crear excel.\n");
    printf("\t 6: Actualizar datos.\n");
    printf("0: SALIR\n");
    scanf("%d", &opcion);
  } while (opcion < 0);
  return opcion;
}

void pressAnyKey () {
  printf("\nPresione Enter para volver al menu ...");
  cleanBuffer();
  getchar();
}