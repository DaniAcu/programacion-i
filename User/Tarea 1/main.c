#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "user.h"

int main() {
  int opcion, id = 1;
  
  while ((opcion = getOption())) {
    switch (opcion) {
      case 1:
        listUsers();
        break;
      case 2:
        createUser(&id);
        break;
      case 3:
        updateUser();
        break;
      case 4:
        deleteUser();
        break;
      case 5:
        createCSV();
        break;
      case 6:
        loadCSV();
        break;
      case 0:
        printf("El programa ha finalizado.\n");
        break;
    }

    pressAnyKey();
  }
  return 0;
}
