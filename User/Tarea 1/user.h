typedef struct {
  int id;
  char name[20];
  int birthday;
  int years;
} User;

void deleteUser();
void updateUser ();
void listUsers ();
void loadCSV();
void createCSV();
void createUser(int *id);
User getUser (int *id);
void showUser (User user);