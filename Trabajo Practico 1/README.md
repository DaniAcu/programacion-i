# Trabajo Práctico Nº 1 
*Programación I*

## Tarea 1:
Escribir un programa que convierta un número dado en segundos en su equivalente en Horas,
Minutos y Segundos. Ejemplo: Segundos=4000, el programa muestra 1H: 6M: 40S.

## Tarea 2:
Un valor de temperatura en grados Celsius puede ser convertida a su equivalente en grados
Fahrenheit de acuerdo a la siguiente fórmula:

>> F = (9/5)C + 32

## Tarea 3
Construir un programa que al recibir como dato un número de 4 dígitos, genere una impresión como
la que se muestra a continuación El número 6352:
6 3 5 2

## Tarea 4:
Se ingresa un valor numérico de 8 dígitos que representa una fecha con el siguiente formato:
aaaammdd
Esto es: los 4 primeros dígitos representan el año, los siguientes 2 representan el mes y los 2 dígitos
restantes representan el día. Se pide informar por separado el día, el mes y el año de la fecha
ingresada.

Por ejemplo:
Dada la fecha con formato aaaammdd:
20200330

El programa debería informar:
Día: 30
Mes: 03
Año: 2020

## ¿Como probar?
Usando el script *run* puede ejecutar todos los programas.

`./run 'Tarea 1'`

Si desea volver a compilar puede usar.

`./compile 'Tarea 2'`

Si estas en **Windows** vas a tener que usar el comando bash para correr estos.
Usando el *cmd* podes correr los comandos asi:

`bash ./run 'Tarea 1'`
-----
`bash ./compile 'Tarea 2'`