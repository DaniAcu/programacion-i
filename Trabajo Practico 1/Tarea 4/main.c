#include <stdio.h>
#include <stdlib.h>

int main() {
  int year, month, day, date;
  
  printf("Ingrese una fecha con el siguiente formato aaaammdd:");
  scanf("%8d", &date);

  day = date % 100;
  date = date / 100;
  month = date % 100;
  year = date / 10000;

  printf("Dia: %02d \n", day);
  printf("Mes: %02d \n", month);
  printf("Año: %04d \n", year);

  return 0;
}