#include <stdio.h>
#include <stdlib.h>

float toFahrenheit (float celsius) {
  return 9.0/5.0 * celsius + 32;
}

int main() {
  float celsius = 0;

  printf("Ingrese una temperatura en C°: ");
  scanf("%f", &celsius);

  float fahrenheit = toFahrenheit(celsius);

  printf("Fahrenheit %.2f", fahrenheit);

  return 0;
}