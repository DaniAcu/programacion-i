#include <stdio.h>
#include <stdlib.h>

int main() {

  int seconds = 0;
  const int TIME_LENGTH = 60;

  printf("Ingrese la cantidad de segundos:");
  scanf("%d", &seconds);

  int minutes = seconds / TIME_LENGTH;
  seconds %= TIME_LENGTH;

  int hours = minutes / TIME_LENGTH;
  minutes %= TIME_LENGTH;

  printf("%dH:%dM:%dS\n", hours, minutes, seconds);

  return 0;
}