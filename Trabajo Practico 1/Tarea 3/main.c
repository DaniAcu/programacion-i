#include <stdio.h>
#include <stdlib.h>

int main() {
  int number = 0;
  
  printf("Ingrese un numero de 4 digitos: ");
  scanf("%4d", &number);

  int digit4 = number % 10;
  number /= 10;
  int digit3 = number % 10;
  number /= 10;
  int digit2 = number % 10;
  number /= 10;
  int digit1 = number % 10;

  printf("%d %d %d %d", digit1, digit2, digit3, digit4);

  return 0;
}